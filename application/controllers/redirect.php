<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: luis.gamero
 * Date: 16.05.14
 * Time: 12:39
 */

class Redirect extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->smarty->assign( 'baseurl', base_url());
        $this->load->model('catalog/catalog_model');
    }

    public function index(){
        $id = $this->input->get('id');
        $product = $this->catalog_model->getProduct_byId($id);
        redirect($product->product_link, 302);

    }


}