<?php
/**
 * Created by PhpStorm.
 * User: luis.gamero
 * Date: 19.05.14
 * Time: 18:43
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Brands extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('brands/brands_model');
        $this->load->library( 'smarty' );
        $this->smarty->assign( 'baseurl', base_url());
    }

    public function index()
    {
        $brands = $this->brands_model->getAllBrands();
        $session = $this->session->userdata('logged_in');

        $this->smarty->assign( 'session', $session );
        $this->smarty->assign( 'brands', $brands );

        $this->smarty->display('layout/all_brands.tpl');



    }


    public function follow_brand() {

        $this->load->model('catalog/catalog_model');
        $this->load->model('category/category_model');
        $session_data = $this->session->userdata('logged_in');

        $brands = $this->brands_model->getQuizBrands((array('brand_style >' => 0)));
        if($this->session->userdata('logged_in')) {
            $session = $this->session->userdata('logged_in');
        }
        foreach ($brands as &$brand)
        {
            $query = array('id' => $brand->id, 'product_gender' => $session['gender']);
            $products = $this->catalog_model->getProductsByBrand($query, null, 4);

                $brand->products = $products;


        }

        $user_brands = ((isset($session_data['brands_id']) ?: $this->catalog_model->getUserBrands($session_data['id'])) );

        $this->smarty->assign( 'categories', $category = $this->category_model->get_all_categories());
        $this->smarty->assign( 'trendbrands', $brands );
        $this->smarty->assign( 'userId', $session_data['id'] );
        $this->smarty->assign( 'brands', $user_brands);
        $this->smarty->assign( 'products', $products);
        $this->smarty->display('brands/follow_brands.tpl');



    }




    public function page($brand_url_key) {

        /* Get the page number from the URI , it has to be the number -1 because later it will be multiply by the number of items per page) */
        $page = ($this->uri->segment(3));
        $this->load->model('catalog/catalog_model');
        $brand_id = $this->brands_model->getIdByUrl($brand_url_key);
        $query = array('product_brand_id' => $brand_id);
        $session = $this->session->userdata('logged_in');


        // Load Pagination
        $this->load->library('pagination');
        // Config setup
        $config['base_url'] = base_url().'shop/'.$brand_url_key;
        $config['first_url'] = '1';
        $config['per_page'] = 30;
        // I added this extra one to control the number of links to show up at each page.
        $config['num_links'] = 10;
        $config['use_page_numbers'] = TRUE;
        $config['total_rows'] =  count($this->catalog_model->getProductsByBrand($query, null, null));
        $offset = ($page  == null) ? 0 : (($page  == 1 ) ? 0 : ($page * $config['per_page']) - $config['per_page']);
        // Initialize
        $this->pagination->initialize($config);

        $products = $this->catalog_model->getProductsByBrand($query, $offset, $config['per_page'],null);

        $this->smarty->assign( 'baseurl', base_url());
        $this->smarty->assign( 'pagination_helper', $this->pagination->create_links());
        $this->smarty->assign( 'products', $products);
        $this->smarty->assign( 'session', $session );
        $this->smarty->display('layout/brand_page.tpl');



    }

    public function getBrandidbyname($name) {
        $result = $this->getByName($name);
        if(!$result) {
            $result = $this->insertBrand($name);
        }
        return $result;
    }

    /**
     * Create url key for all brands
     */
    public function createUrlKey() {

        $data = $this->brands_model->getAllBrands();
        foreach($data as  $string) {
            $brand = str_replace(' ', '-', $string->name); // Replaces all spaces with hyphens.
            $brand = strtolower($brand);
            $brand = preg_replace('/[^A-Za-z0-9\-]/', '', $brand); // Removes special chars.
            $url_key =  preg_replace('/-+/', '-', $brand). "-shop"; // Replaces multiple hyphens with single one.

            $product = array(
                'id' => $string->id,
                'url_key' => $url_key
            );

            echo $this->brands_model->updateBrand($product);

        }

    }

    public function removeBrandProducts(){
        $brandId = $this->input->post('brand');
        return $this->brands_model->deletebrandproducts($brandId);
    }

    public function removeItem(){
        $id = $this->input->post('brand');
        $this->load->model('catalog/catalog_model');
        return $this->catalog_model->delete($id);
    }

} 