<?php
/**
 * Created by PhpStorm.
 * User: luis.gamero
 * Date: 23.05.14
 * Time: 15:46
 */

class Quiz extends CI_Controller{
    public function __construct()
    {
        parent::__construct();
        // Load libraries
        $this->load->database();
        // Load helpers
        $this->load->helper('url');
        $this->load->model('user/user_model');
    }

    private  $allow_guest_user = TRUE;

    public function index()
    {
        $this->load->model('brands/brands_model');
        $data = $this->input->post();
        $data['brands'] = $this->brands_model->getBrands(null);
        $data['gender'] = $this->input->post('gender');
        log_message('debug', 'The purpose of some variable is to provide some value.');
        $this->load->view('layout/header');
        $this->load->view('register/survey_wizard', $data);
        $this->load->view('layout/footer');
    }

    public function stepOne()
    {
        $this->load->model('styles/styles_model');
        $this->load->model('catalog/catalog_model');
        $this->smarty->assign( 'gender', $this->input->post('gender'));
        $this->smarty->assign( 'styles', $this->styles_model->get_all_styles());
        $this->smarty->assign( 'baseurl', base_url());
        $this->smarty->assign( 'products', $this->catalog_model->get_favorite_products());
        $this->smarty->display('register/survey/one.tpl');
    }


    public function stepTwo()
    {
        $this->load->model('brands/brands_model');
        $this->load->model('catalog/catalog_model');
        $products = array();
        $session = $this->session->userdata('user_data');
        $query = array('status' => 1, 'typebrand' =>  'quiz');
        $brands = $this->brands_model->getQuizBrands($query);
        foreach ($brands  as $key => $value ) {
            $query = array('product_brand_id' => $value->id, 'product_gender' => $session['gender']);
            if($this->catalog_model->getProductsByBrand($query)){
                $products[$value->name][] = $this->catalog_model->getProductsByBrand($query,null,4);
            }
        }

        $this->smarty->assign( 'products', $products);
        $this->smarty->assign( 'brands', $brands);
        $this->smarty->assign( 'baseurl', base_url());
        $this->smarty->display('register/survey/two.tpl');

    }

    public function stepThree()
    {
        $this->load->model('catalog/catalog_model');
        $session_data = $this->session->userdata('user_data');
        $logged_in = $this->session->userdata('logged_in');
        $this->smarty->assign( 'products', $this->catalog_model->get_favorite_products());

        if($logged_in)
        {
            $userId = $logged_in['id'];
        }
        else {
            if($this->allow_guest_user)
            {
                $userId = $this->createGuestUser();
                $this->smarty->assign( 'user_id', $userId);
            }


        }

        $this->smarty->assign( 'user_id', null);
        $this->smarty->assign( 'baseurl', base_url());
        $this->smarty->display('register/survey/three.tpl');

    }


    public function stepFour()
    {
        $this->load->view('layout/header');
        $this->load->view('register/survey/four');
        $this->load->view('layout/footer');

    }


    /**
     * creates a guest user in database
     */

    public function createGuestUser(){
        $session = $this->session->userdata('user_data');


        if($this->input->post("user"))
        {
            $user = $this->input->post("user");
        }
        else  {
            $user = array(
                'email'=>null,
                'gender'=>$session['gender'],
                'username'=>$session["id"],
                'password'=>md5($session["id"])
            );


        }


        $user_id= $this->user_model->createUser($user);
        var_dump($user_id);

        if($user_id)
        {
            foreach($session['brands_id'] as $brand){
                $this->user_model->saveBrandToUser($user_id,$brand);
            }

            foreach($session["style_id"] as $style){
                $this->user_model->saveStyleToUser($user_id, $style);
            }

            $userData = $this->user_model->getUserProfile($user_id);

            $sess_array = array(
                'id' => $user_id,
                'username' => $userData->username,
                'gender' => $userData->gender
            );
            $this->session->set_userdata('logged_in', $sess_array);

            $email = $userData->email;
            if($email){
                $this->sendConfirmation($email);
                redirect(base_url().'catalog/showroom', 302);
                //redirect(base_url().'register/quiz/successful?token='.$email, 302);
            }

            return $user_id;

        }
        else
        {
            //redirect(base_url()/'register/quiz', 302);
        }
    }


    /**
     * Function which inserts user data into database
     */
    public function register()
    {
        $data['user'] = $this->input->post("user");
        $user_id= $this->user_model->createUser($data['user']);

        if($user_id)
        {
            $sess_array = array(
                'id' => $user_id,
                'username' => $data['user']['username']
            );
            $this->session->set_userdata('logged_in', $sess_array);

            $email = $data['user']['email'];
            $this->sendConfirmation($email);
            redirect(base_url().'catalog/showroom', 302);
            //redirect(base_url().'register/quiz/successful?token='.$email, 302);
        }
        else
        {
            redirect(base_url()/'register/quiz', 302);
        }
    }



    /**
     * Creates a session variable with some basic user temporary data
     * @return bool
     */
    public function createTempUser(){
        $stylesId = $this->input->post('style');
        $gender = $this->input->post('gender');
        if(!$this->session->userdata('user_data')){
            $id_user = mt_rand();
            $sess_array = array(
                'id' => $id_user,
                'gender' => $gender,
                'style_id' => $stylesId,
                'brands_id' => NULL,
                'age' => NULL,
                'colors' => NULL,
            );
            $this->session->set_userdata('user_data', $sess_array);
            session_start();




            return true;
        }
        else {
            $this->editStyle($stylesId, $gender);
        }

    }



    public function editStyle($stylesId, $gender){
        $session_data = $this->session->userdata('user_data');
        $session_data['style_id'] = $stylesId;
        $session_data['gender'] = $gender;

        $this->session->set_userdata('user_data',$session_data);
        return true;
    }

    public function editBrands(){
        $brands = $this->input->post('brands');
        $session_data = $this->session->userdata('user_data');
        $session_data['brands_id'] = $brands;
        $this->session->set_userdata('user_data',$session_data);


        return true;

    }

    public function editAge($age){
        $session_data = $this->session->userdata('user_data');
        $session_data['age'] = $age;
        return true;
    }

    public function editColors($colors){
        $session_data = $this->session->userdata('user_data');
        $session_data['colors'] = $colors;
        return true;
    }

    /**
     * Creates a temporal login session
     */
    protected function createLoginSession($session){
        $sess_array = array(
            'id' => $session['id'],
            'brands_id' => $session['brands_id'],
        );
        session_destroy();

        $this->session->set_userdata('logged_in', $sess_array);
        session_start();
        return $this->session->userdata('logged_in');
    }






    public function followbrand()
    {
        $brandId = $this->input->post('brand');
        $userId = $this->input->post('user');
        $result = $this->user_model->saveBrandToUser($userId,$brandId);
        return true;
    }

    public function successful() {
        $this->load->view('layout/header');
        $this->load->view('register/success');
        $this->load->view('layout/footer');
    }




    public function sendConfirmation($user_email) {
        /* Empfänger */
        $empfaenger = $user_email;

        /* Absender */
        $absender = 'kontakt@styluv.de';

        /* Betreff */
        $subject = 'Welcome to STYLUV, your personal style room';

        /* Nachricht */
        $message = 'Thank you for registering to the beta version of Styluv';

        /* Baut Header der Mail zusammen */
        $header = ('From:' . $absender . ' \n');
        $header .= ('Reply-To: ' . $absender . ' \n');
        $header .= ('Return-Path: ' . $absender . ' \n');
        $header .= ('X-Mailer: PHP/' . phpversion() . ' \n');
        $header .= ('X-Sender-IP: ' . $REMOTE_ADDR . ' \n');
        $header .= ('Content-type: text/html \n');

        /* Verschicken der Mail */
        mail($empfaenger, $subject, $message, $header, '-f kontakt@styluv.de');

        echo 'Die E-Mail wurde versendet.';

    }

    public function feedback(){

        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'smtp.mandrillapp.com',
            'smtp_port' => 587,
            'smtp_user' => 'luardohm@gmail.com',
            'smtp_pass' => 'AfDiG6phlHaZwJ3Q0XdwaA',
            'mailtype'  => 'html'
        );

        //load email helper
        $this->load->helper('email');
        //load email library
        $this->load->library('email', $config);

        $to = "luardohm@gmail.com";
        $email = "interoom@hotmail.com'";
        $comment = $this->input->post('comment');

        // check is email addrress valid or no
        if (valid_email($email)){
            // compose email
            $this->email->from($email , 'Test User');
            $this->email->to($to);
            $this->email->subject('Feedback Registration Styluv');
            $this->email->message($comment);

            // try send mail ant if not able print debug
            if ( ! $this->email->send())
            {
                $data['message'] ="Email not sent \n".$this->email->print_debugger();
                echo "not sent";
                return false;
            }
            // successfull message
            $data['message'] ="Email was successfully sent to $email";
            return true;
        } else {

            $data['message'] ="Email address ($email) is not correct. Please <a href=".base_url().">try again</a>";

            $this->load->view('layout/header');
            $this->load->view('register/success',$data);
            $this->load->view('layout/footer');
        }


    }

    public function yourstyle() {
        $this->load->view('layout/header');
        $this->load->view('register/survey_wizard', $data);
        $this->load->view('layout/footer');
    }

} 