<?php
/**
 * Created by PhpStorm.
 * User: luis.gamero
 * Date: 02.06.14
 * Time: 11:46
 */

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Feedback extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
    }



    public function feedbackpost(){

        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'mail-prod-01.vidamo.cd.net.de',
            'smtp_port' => 25,
            'smtp_user' => 'service@vidamo.de',
            'smtp_pass' => 'LteafOoauu',
            'mailtype'  => 'html'
        );

        //load email helper
        $this->load->helper('email');
        //load email library
        $this->load->library('email', $config);

        $to = "interoom@hotmail.com";
        $email = $this->input->post('email');
        $comment = $this->input->post('comment');

        if(! isset ($email)){
            $email = "testuser@styluv.de";
        }

        // check is email addrress valid or no
        if (valid_email($email)){
            // compose email
            $this->email->from($email , 'Test User');
            $this->email->to($to);
            $this->email->subject('Feedback Registration Styluv');
            $this->email->message($comment);

            // try send mail ant if not able print debug
            if ( ! $this->email->send())
            {
                $data['message'] ="Email not sent \n".$this->email->print_debugger();
                return false;
            }
            // successfull message
            $data['message'] ="Email was successfully sent to $email";
            return true;
        } else {

            $data['message'] ="Email address ($email) is not correct. Please <a href=".base_url().">try again</a>";

            $this->load->view('header');
            $this->load->view('success',$data);
            $this->load->view('footer');
        }


    }


}