<?php
/**
 * Created by PhpStorm.
 * User: luis.gamero
 * Date: 28.07.14
 * Time: 21:59
 */

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        // Load helpers
        $this->load->helper('url');
        $this->load->model('user/user_model');
    }


    public function edit_user_Style($styleId, $gender){
        $session_data = $this->session->userdata('user_data');
        $session_data['style_id'] = $styleId;
        $session_data['gender'] = $gender;
        $this->session->set_userdata('user_data',$session_data);
        return true;
    }

    public function edit_user_Brands(){
        $brands = $this->input->post('brands');
        $session_data = $this->session->userdata('user_data');
        $session_data['brands_id'] = $brands;
        $this->session->set_userdata('user_data',$session_data);
        return true;

    }

    public function edit_user_Age($age){
        $session_data = $this->session->userdata('user_data');
        $session_data['age'] = $age;
        return true;
    }

    public function edit_user_Colors($colors){
        $session_data = $this->session->userdata('user_data');
        $session_data['colors'] = $colors;
        return true;
    }

    /**
     * Creates a temporal login session
     */
    protected function createLoginSession($session){
        $sess_array = array(
            'id' => $session['id'],
            'brands_id' => $session['brands_id'],
        );
        session_destroy();

        $this->session->set_userdata('logged_in', $sess_array);
        session_start();
        return $this->session->userdata('logged_in');
    }



}