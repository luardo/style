<?php
/**
 * Created by PhpStorm.
 * User: luis.gamero
 * Date: 20.05.14
 * Time: 12:28
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        // Load libraries
        $this->load->database();

        // Load helpers
        $this->load->helper('url');
        $this->load->model('user/user_model');
    }

    public function index()
    {
        //$this->load->helper(array('form'));
        $this->load->view('layout/header');
        $this->load->view('user/login_view');
        $this->load->view('layout/footer');

    }

    function logout()
    {
        $this->session->unset_userdata('logged_in');
        session_destroy();
        redirect(base_url(), 302);
    }





}