<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: luis.gamero
 * Date: 16.05.14
 * Time: 12:39
 */

class Home extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library( 'smarty' );
        $this->smarty->assign( 'baseurl', base_url());
    }

    public function index()
    {

        $this->load->model('brands/brands_model');
        //$data['brands'] = $this->brands_model->getBrands(null);

        $this->smarty->assign( 'baseurl', base_url());
        $this->smarty->display('home.tpl');
    }

    public  function about(){

        $this->smarty->assign( 'baseurl', base_url());
        $this->smarty->display('about-us.tpl');
    }

    public  function contact(){
        $this->smarty->display('contact.tpl');
    }

    public  function impressum(){
        $this->smarty->assign( 'baseurl', base_url());
        $this->smarty->display('impressum.tpl');
    }



} 