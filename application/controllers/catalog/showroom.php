<?php
/**
 * Created by PhpStorm.
 * User: luis.gamero
 * Date: 20.06.14
 * Time: 11:40
 */

class Showroom extends CI_Controller {



    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('catalog/catalog_model');
        $this->load->model('category/category_model');
        $this->load->model('brands/brands_model');
        $this->load->library('pagination');

    }



    public function index($offset = 0)
    {

        if($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            /* Get the page number from the URI , it has to be the number -1 because later it will be multiply by the number of items per page) */
            $page = ($this->uri->segment(3));
            $param = ($this->uri->segment(1))? $this->uri->segment(1) : 'showroom';

            //$page = $this->input->get('p');
            $queryArray = $this->getParams($this->input->get());

            // Config setup
            $config['base_url'] = base_url().$param.'/p';
            $config['first_url'] = '1';

            $config['per_page'] = 64;
            $config['use_page_numbers'] = TRUE;
            $offset = ($page  == null) ? 0 : (($page  == 1 ) ? 0 : ($page * $config['per_page']) - $config['per_page']);
            // I added this extra one to control the number of links to show up at each page.
            $config['num_links'] = 10;




            $trendBrands = $this->get_brands_suggestion();


            switch ($param)
            {
                case 'showroom':

                    if($queryArray)
                    {
                        $products   = $this->catalog_model->getProducts($session_data['id'], $offset, $config['per_page'], $queryArray);
                        $config['total_rows'] =  count($this->catalog_model->getProducts($session_data['id'], null, null,$queryArray));
                    }
                    else {
                        $products = $this->catalog_model->getProducts_byStyle($session_data['id'], $offset, $config['per_page']);
                        $config['total_rows'] =  count($this->catalog_model->getProducts_byStyle($session_data['id'], null, null,null));
                    }

                    break;

                case 'new':
                    $products = $this->catalog_model->get_new_Products($session_data['id'], $offset, $config['per_page']);
                    $config['total_rows'] =  count($this->catalog_model->get_new_Products($session_data['id'], null, null,null));

                    break;

                case 'favorites':
                    $products   = $this->catalog_model->get_favorite_products($session_data['id']);
                    $luv_products   = $this->catalog_model->get_favorite_products();
                    $this->smarty->assign( 'luv_products', $luv_products);
                    $trendBrands = null;

                    break;

                case 'popular':
                    $products   = $this->catalog_model->get_favorite_products($session_data['id']);
                    $this->smarty->assign( 'luv_products', $luv_products);

                    break;

                default:
                    if($queryArray) {
                        $products = $this->catalog_model->getProducts($session_data['id'], $offset, $config['per_page'], $queryArray);
                        $config['total_rows'] = count($this->catalog_model->getProducts($session_data['id'], null, null, $queryArray));
                    } else {
                        $products = $this->catalog_model->getProducts_byStyle($session_data['id'], $offset, $config['per_page']);
                        $config['total_rows'] = count($this->catalog_model->getProducts_byStyle($session_data['id'], null, null, null));
                    }
                    break;
            }

            // Initialize
            $this->pagination->initialize($config);


            $brands     = ((isset($session_data['brands_id']) ?: $this->catalog_model->getUserBrands($session_data['id'])) );



            $this->smarty->assign( 'pagination_helper', $this->pagination->create_links());
            $this->smarty->assign( 'categories', $category = $this->category_model->get_all_categories());
            $this->smarty->assign( 'baseurl', base_url());
            $this->smarty->assign( 'userId', $session_data['id'] );
            $this->smarty->assign( 'trendbrands', $trendBrands);
            $this->smarty->assign( 'session', $this->session->userdata('logged_in'));
            $this->smarty->assign( 'brands', $brands );
            $this->smarty->assign( 'products', $products);
            $this->pagination->initialize($config);
            $this->smarty->display('showroom.tpl');

        } else {
            //If no session, redirect to login page
            $this->smarty->assign( 'baseurl', base_url());
            $this->smarty->display('home.tpl');
        }

    }




    public function get_brands_suggestion()
    {
        $brands = $this->brands_model->getQuizBrands((array('typebrand' => 'sidebar')),3);
        foreach ($brands as &$brand)
        {
            $query = array('id' => $brand->id);
            $products = $this->catalog_model->getProductsByBrand($query, 8, 4);
            $brand->products = $products;
        }

        return $brands;
    }


    /**
     * get the Get parameters
     */

    public function getParams($params){

        if($params)
        {
            $query = array();
            foreach($params as $key => $val)
            {
                switch($key){
                    case 'brand':
                        $query['id']= $val;
                        break;
                    case 'order':
                        $query['order'] =  $val;
                        break;
                    case 'style':
                        //$query = array('style_id' => $val);
                        break;
                    case 'cat':
                        $query['product_category'] = $val;
                        break;
                    case 'sale':
                        $query['product_old_price >'] = 0;
                        break;

                }


            }

            return $query;
        }

    }





    public function user_likes_product()
    {
        $this->load->model('user/user_model');
        if($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
        }
        $product_id = $this->input->post('productId');
        $user_id = $session_data['id'];

        if($this->user_model->user_like_product($user_id, $product_id))
        {
            return true;
        }

    }




    public function userfollowsbrand($brand){
        $users_brands = $this->catalog_model->getUserBrands();
        foreach ($users_brands as $user_brand) {
            if(strtolower($user_brand->name) == strtolower($brand)){
                return true;
            }
            else {
                false;
            }
        }

    }



}