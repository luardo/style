<?php

class productfeed extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->database();

        // Load helpers
        $this->load->helper('url');
        $this->load->model('catalog/catalog_model');
        $this->load->library('table');
    }

    public function index(){
        //$csvFile="zxpd_201408200813_9356_28494037.csv";
        //$products = $this->getProductFeed($csvFile);
        $csvFiles = array(
            array(
                'file' => 'soliver.csv',
                'store' => 's.Oliver'
            ),
            array(
                'file' => 'joop.csv',
                'store' => 'Joop'
            ),
            array(
                'file' => 'armani.csv',
                'store' => 'Armani'
            ),
            array(
                'file' => 'wefashion.csv',
                'store' => 'WE Fashion'
            ),
            array(
                'file' => 'styleicone.csv',
                'store' => 'STYLEICÔNE'
            ),
            array(
                'file' => 'forever21.csv',
                'store' => 'Forever 21'
            ),
            array(
                'file' => 'mytheresa.csv',
                'store' => 'My Theresa'
            ),
            array(
                'file' => 'missselfridge.csv',
                'store' => 'Miss Selfridge'
            ),
            array(
                'file' => 'topshop.csv',
                'store' => 'Topshop'
            ),
            array(
                'file' => 'urbanoutfitters.csv',
                'store' => 'Urban Outfitters'
            ),
            array(
                'file' => 'allsaints.csv',
                'store' => 'All Saints'
            ),
            array(
                'file' => 'girissima.csv',
                'store' => 'Girissima'
            ),array(
                'file' => 'jades24.csv',
                'store' => 'Jades24'
            ),array(
                'file' => 'tommy.csv',
                'store' => 'Tommy Hilfiger'
            ),

        );


        $csvFile = $this->input->get('csv');
        $csvStore = $this->input->get('store');

        if(($csvFile))
        {
            $products = $this->getProductFeed($csvFile);
            $this->insertProducts($products, $csvStore);
        }

        $this->smarty->assign( 'baseurl', base_url());

        $this->smarty->assign( 'csvFiles', $csvFiles);
        $this->smarty->display('feed/feed.tpl');

    }


    public function getProductFeed($csvFile) {
        $this->load->library('csvreader');
        $filePath = $_SERVER['DOCUMENT_ROOT'].'/assets/csv/'.$csvFile;
        $result =   $this->csvreader->parse_file($filePath);
        return $result;

    }


    public function insertProducts($products, $retailer){
        $this->load->model('brands/brands_model');
        $this->load->model('retailer/retailer_model');
        $inserted_products = 0;
        $updated_products = 0;
        foreach($products as $key => $product){

                if(isset($product['oldprice']) || $product['oldprice']!= 0): $oldprice = $product['oldprice']; else: $oldprice = null; endif;
                if(isset($product['MerchantProductCategory'])): $MerchantProductCategory = $product['MerchantProductCategory']; else: $MerchantProductCategory = ''; endif;
                if(isset($product['UpdateDate'])): $updateDate =  $this->convertToDate($product['UpdateDate']); else: $updateDate =  null;  endif;


            $product_category = $this->getCategoryId($product['MerchantProductCategory']);

            if(!$product_category){
                $product_category = $this->getCategoryId($product['name']);
            }


                $data = array(
                    'product_name' => $product['name'],
                    //'product_Zupid' => $product['Zupid'],
                    'product_brand_id' => $this->brands_model->insertBrand($product['brand']),
                    'product_old_price' => $oldprice,
                    'product_price' => $product['price'],
                    'product_MerchantProductNumber' => $product['MerchantProductNumber'],
                    'product_link' => $product['link'],
                    'product_image_small' => $product['imgsmall'],
                    'product_image_medium' => $product['imgmedium'],
                    'product_category' => $product_category,
                    'product_gender' => $this->getGender($MerchantProductCategory, $product['link']),
                    'product_status' => 1,
                    'product_UpdateDate' => $updateDate,
                    'product_shop' => $this->retailer_model->insertRetailer($retailer),
                );

                $productId = $this->catalog_model->isProductDuplicated($data);
                if($productId){
                    $this->catalog_model->updateProducts($productId, $data);
                    $updated_products++;
                }
                else {
                    $this->catalog_model->saveProducts($data);
                    $inserted_products++;
                }


        }
        echo $inserted_products. " products updated <br />";
        echo $inserted_products. " products inserted <br />";
    }

    protected function getCategoryId($string){

        $this->load->model('category/category_model');
        $separators = array('|','>',' ');
        $string = str_replace($separators, "/", $string);
        $categoriesString = explode('/',$string);

        $categoryId = null;

        if(!is_array($categoriesString)){
            $category = $this->category_model->getCategoryId_ByString($categoriesString);
            if($category)
            {
                $categoryId = $category;
            }
        }

        else {
            foreach($categoriesString as $categoryString)
            {
                $categoryString = preg_replace('/\s+/', '', $categoryString);
                $category = $this->category_model->getCategoryId_ByString($categoryString);
                if($category)
                {
                    $categoryId = $category;
                }
            }
        }


        return $categoryId;


    }

    protected function getGender($string, $stringURL){
        $string= strtolower($string);
        $stringURL= strtolower($stringURL);
        if(preg_match("/\b(damen|sie|women|frau)\b/", $string) || preg_match("/\b(damen|sie|women|frau)\b/", $stringURL)){
            return 1;
        }

        if(preg_match("/\b(herren|ihn|men|women|mann)\b/", $string) || preg_match("/\b(herren|ihn|men|mens|mann)\b/", $stringURL)){
            return 0;
        }
        else {
            return 1;
        }



    }

    protected function convertToDate($date){
        $date = str_replace('.','-', $date);
        //echo date_format(new DateTime($date), 'd/m/y');
// create date object by using a known format
// see manual page for all format specifiers
        $d = DateTime::createFromFormat('d/m/y', $date);

// format date object in yyyy-mm-dd
        return $d->format('Y-m-d');

    }



    public function createbrandURL($brand){
        $brand = str_replace(' ', '-', $brand); // Replaces all spaces with hyphens.
        $brand = strtolower($brand);
        $brand = preg_replace('/[^A-Za-z0-9\-]/', '', $brand); // Removes special chars.
        $url_key =  preg_replace('/-+/', '-', $brand). "-shop"; // Replaces multiple hyphens with single one.
        return $url_key;
    }









}