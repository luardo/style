<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "catalog/showroom";
$route['404_override'] = '';
$route['about'] = 'home/about';
$route['impressum'] = 'home/impressum';
$route['contact'] = 'home/contact';
$route['showroom/page/(:num)'] = 'catalog/showroom';

$route['showroom'] = 'catalog/showroom/index/1';
$route['showroom/p/'] = 'catalog/showroom/index/1';
$route['showroom/p/(:num)'] = 'catalog/showroom/index/$1/';

$route['favorites'] = 'catalog/showroom/index/1';
$route['favorites/p/(:num)'] = 'catalog/showroom/index/$1/';
$route['new'] = 'catalog/showroom/index/1';
$route['new/p/(:num)'] = 'catalog/showroom/index/$1/';

$route['brands-to-follow'] = "brands/brands/follow_brand";

$route['shop/(:any)/(:num)'] = 'brands/brands/page/$1/$2/';

$route['user/logout'] = 'user/login/logout';
$route['shop/(:any)'] = "brands/brands/page/$1/";
$route['register/quiz'] =  "register/quiz";
$route['all-brands'] = "brands/brands";

/* End of file routes.php */
/* Location: ./application/config/routes.php */