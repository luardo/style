{extends file="layout_2cols.tpl"}
{block name="content"}
    <div class="cms-content">
            <h1>About us</h1>

            <p>Styluv ist eine Plattform für kuratiertes Onlineshopping, die modebegeisterte mit ihren Lieblingsmarken verbindet. Styluv bietet ein persönliches Einkaufserlebnis mit der größten Auswahl an Marken und Trends.</p>

            <h4>Dein persönlicher Showroom</h4>

            <p>Folge Deinen Lieblingsmarken und wähle den Style, der am Besten zu Dir passt. Dann findest Du in Deinem Showroom die beste Auswahl an Trends, die genau zu Dir passen. </p>

            <h4>Lass Dich inspirieren</h4>

            <p>Entdecke jeden Tag neue Modetrends, Kollektionen und lass Dich von den Looks und Outfits inspirieren, die unsere Stylisten für Dich entwickelt haben. </p>

            <h4>Shop</h4>

            <p>Unsere einfache und intuitive Navigation hilft Dir die besten Produkte zu finden. Du musst nicht mehr durch unzählige Shops surfen. Mit Styluv findest Du genau das was Du suchst – in Deinem persönlichen Showroom. </p>

            <h4>Besser als jedes Outlet</h4>

            <p>Verpass keinen Sale mehr. Wir benachrichtigen Dich, wenn eine Deiner Lieblingsmarken reduziert wird. Ob Schlussverkauf, Sonderaktionen oder Topangebote: Mit Styluv wirst Du es als Erste erfahren.  </p>

            <h4>Unsere Technologie</h4>

            <p>Wir nutzen die besten Technologien zum Erkennen von Produkten und Bildern und entwickeln Sie ständig weiter, um Dir das bestmögliche individuelle Angebot anbieten zu können. Unser Ziel ist es, Dir für jedes Produkt das Du wählst immer die am Besten passenden ähnlichen Produkte und Marken zeigen zu könne. </p>

        </div>
{/block}