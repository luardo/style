{extends file="layout.tpl"}
{block name="content"}
    <div class="showroom-header">
        <h2 class="text-center">My Style Feed</h2>
        <div class="navigation small-centered">
            <form method="get">
                <ul>

                    <li class="active">
                        <a href="{$baseurl}">Showroom</a>
                        <div class="box-filter scrollslider ">
                            <div class="search-filter">
                                <input type="text" placeholder="search" />
                            </div>
                            <div class="scroll-bar mCustomScrollbar" data-mcs-theme="dark-3">
                                <ul class="filters">
                                    {foreach $categories as $category}
                                        <li><a href="?cat={$category->cat_id}"> {$category->cat_name} </a></li>
                                    {/foreach}
                                </ul>
                            </div>
                        </div>

                    </li>

                    <li><a href="{$baseurl}new">All new</a> </li>
                    <li><a href="{$baseurl}favorites">Popular</a> </li>
                    <li><a href="{$baseurl}brands-to-follow">Follow Brands</a>

                        <div class="box-filter scrollslider ">
                            <div class="search-filter">
                                <input type="text" placeholder="search" />
                            </div>
                            <div class="scroll-bar mCustomScrollbar" data-mcs-theme="dark-3">
                                <ul class="filters">
                                    {foreach $brands as $brand}
                                        <li><a href="?brand={$brand->brand_id}"> {$brand->name} </a></li>
                                    {/foreach}
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li><a href="/?sale=1">On sale</a></li>

                </ul>
            </form>
        </div>
    </div>

    <div class="row products">


        <div id="container" class="large-12 collapse right">


            <div class="row">

                <div class="markenbox">
                    <ul>
                        {foreach $trendbrands as $trendbrand}
                            <li>
                                <div class="title-brand">
                                    <span class="brand"><a href="?brand={$trendbrand->id}" >{$trendbrand->name}</a> </span>

                                    <label for="{$trendbrand->id}">
                                        <input type="checkbox" class="brand" value="{$trendbrand->id}" id="{$trendbrand->id}" name="brand">
                                        <input type="hidden" class="user" value="{$userId}" name="userId">

                                        <button href="#" title="" class=" button follow btn-follow">Follow</button>

                                    </label>

                                </div>
                                {foreach $trendbrand->products as $product }
                                    <div class="itembox">
                                        <div class="item">
                                            <img data-original="{$product->product_image_small}" class="lazy" src="{$baseurl}assets/img/spinner.gif" />
                                        </div>
                                    </div>
                                {/foreach}

                            </li>
                        {/foreach}
                    </ul>

                </div>



                {foreach $products as $product}
                <div class="box">
                    <div class="love-box">
                       <button class="btn luv-it" data="{$product->id_product}">LUV IT</button>
                        <br />
                        <a href="{base_url()}redirect/?id={$product->id_product}" target="_blank">
                        <button class="btn buy-it">BUY IT</button>
                        </a>
                    </div>

                    <div class="image-container" style="overflow: hidden">
                        <a href="{base_url()}redirect/?id={$product->id_product}" target="_blank">
                            <img class="lazy" data-original="{$product->product_image_medium}"
                                 src="{base_url()}assets/img/spinner.gif">
                        </a>
                    </div>

                    <div class="panel">
                        <div class="product-brand">{$product->name}</div>
                        <div class="product-name"><a href="{$product->id_product}"
                                                     target="_blank">{$product->product_name|truncate:35}</a></div>
                        <div class="product-price">
                            {if ($product->product_old_price > 0)}
                            <span class="old-price">{$product->product_old_price}€</span>
                            Now: {$product->product_price} €
                            {else}
                            {$product->product_price} €
                        {/if}
                        </div>

                            {if isset($session)}
                                {if $session["username"] eq 'luardo123'}
                                    <a class="luv" value="{$product->id_product}">Highlight</a>
                                    |
                                    <a class="delete" value="{$product->id_product}">Delete</a>
                                {/if}
                            {/if}

                        </div>

                    </div>
                {/foreach}


        </div>


            {if isset($luv_products)}
            <div class="row">

                <h3>All LUV from the community</h3>


                {foreach $luv_products as $product}
                    <div class="box">
                        <div class="love-box">
                            <button class="btn luv-it" data="{$product->id_product}">LUV IT</button>
                            <br />
                            <a href="{base_url()}redirect/?id={$product->id_product}" target="_blank">
                                <button class="btn buy-it">BUY IT</button>
                            </a>
                        </div>

                        <div class="image-container" style="overflow: hidden">
                            <a href="{base_url()}redirect/?id={$product->id_product}" target="_blank">
                                <img class="lazy" data-original="{$product->product_image_medium}"
                                     src="{base_url()}assets/img/spinner.gif">
                            </a>
                        </div>

                        <div class="panel">
                            <div class="product-brand">{$product->name}</div>
                            <div class="product-name"><a href="{$product->id_product}"
                                                         target="_blank">{$product->product_name|truncate:35}</a></div>
                            <div class="product-price">
                                {if ($product->product_old_price > 0)}
                                    <span class="old-price">{$product->product_old_price}€</span>
                                    Now: {$product->product_price} €
                                {else}
                                    {$product->product_price} €
                                {/if}
                            </div>

                            {if isset($session)}
                                {if $session["username"] eq 'luardo123'}
                                    <a class="luv" value="{$product->id_product}">Highlight</a>
                                    |
                                    <a class="delete" value="{$product->id_product}">Delete</a>
                                {/if}
                            {/if}

                        </div>

                    </div>
                {/foreach}
            </div>


        </div>

        {/if}

    </div>
    <div class="row pagination pagination-centered">
        {$pagination_helper}
    </div>

    <script type="text/javascript">
        //initiating jQuery
        jQuery(function($) {
            $(document).ready( function() {
                //enabling stickUp on the '.navbar-wrapper' class
                $('.navigation').stickUp();
            });
        });

    </script>

{/block}