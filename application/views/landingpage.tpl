<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Latest fashion collections, season trends and designer fashion from all the world in one place - Styluv. Love it, style it</title>
    <link href='http://fonts.googleapis.com/css?family=Julius+Sans+One' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Hammersmith+One' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{$baseurl}/assets/css/foundation.css" />
    <link rel="stylesheet" href="{$baseurl}/assets/css/jquery.mCustomScrollbar.css" />
    <link rel="stylesheet" href="{$baseurl}/assets/css/style.css" />
    <link rel="stylesheet" href="{$baseurl}/assets/css/sprites.css" />
    <script src="{$baseurl}/assets/js/vendor/modernizr.js"></script>
    <script src="{$baseurl}/assets/js/jquery.min.js"></script>

    <script src="{$baseurl}/assets/js/vendor/jquery.cookie.js"></script>
    <script src="{$baseurl}/assets/js/vendor/jquery.lazyload.js"></script>
    <script src="{$baseurl}/assets/js/stickUp.min.js"></script>
    <meta name="verification" content="adbf4ef53034297f79b11d03ef6d4c6d" />
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">



    <!-- KISSmetrics tracking snippet -->
    <script type="text/javascript">var _kmq = _kmq || [];
        var _kmk = _kmk || 'b296dc1a6b829846ea9162ba0ab78967bcaa28a6';
        function _kms(u){
            setTimeout(function(){
                var d = document, f = d.getElementsByTagName('script')[0],
                        s = d.createElement('script');
                s.type = 'text/javascript'; s.async = true; s.src = u;
                f.parentNode.insertBefore(s, f);
            }, 1);
        }
        _kms('//i.kissmetrics.com/i.js');
        _kms('//doug1izaerwt3.cloudfront.net/' + _kmk + '.1.js');
    </script>


</head>
<body class="landing-page">

<div class="header-banner">

    <div class="header-content">
        <div class="title-box">

            <div class="logo-container">
                <div class="logo">
                    <a href="{$baseurl}"> <img src="{$baseurl}assets/img/logo-big.png"
                                               alt="Follow your favorite brands: Gucci, Chloe, Armani, Burbery and more at Styluv" />
                    </a>
                </div>



            </div>

            <div class="slogan">
                <h3>Finde Mode die Du liebst! </h3>
                <p>
                    Ausgewählte Kollektionen und Trends nur für Dich
                </p>

                <ul>
                    <li><i class="fa fa-check"></i> Perfekt abgestimmte Outfits </li>
                    <li><i class="fa fa-check"></i> Gratis Beratung & Versand</li>
                    <li><i class="fa fa-check"></i> In aller Ruhe ausprobieren</li>

                </ul>

            </div>




            <form action="{$baseurl}register/quiz/stepone" method="post">
                <input type="hidden" name="gender" value="1" />
                <input type="submit" class="call-to-action" value="Jetzt dein style erstellen" />
            </form>

            <small>
                Already a member? <a href="#">Log in</a>
            </small>

            <div  class="social-media">
                <span class="ico-Facebook"></span>
                <span class="ico-Pinterest"></span>
                <span class="ico-Twitter"></span>

                <span class="ico-Google_plus"></span>
                <span class="ico-Instagram"></span>

            </div>
        </div>
    </div>



</div>


<div class="wrapper">
    {block name="content"}{/block}



    <footer class="row">
        <div class="large-12 columns">
            <hr>

            <div class="row">
                <div class="large-6 columns">
                    <p></p>
                </div>

                <div class="large-6 columns">
                    <ul class="inline-list right">
                        <li>
                            <a href="{$baseurl}contact">Kontakt</a>
                        </li>

                        <li>
                            <a href="{$baseurl}about">Über uns</a>
                        </li>

                        <li>
                            <a href="{$baseurl}impressum">Impressum</a>
                        </li>

                    </ul>
                </div>
            </div>
        </div>
    </footer>

</div>




</body>
</html>