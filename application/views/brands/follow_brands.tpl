{extends file="layout.tpl"}
{block name="content"}

    <div class="showroom-header">
        <h2 class="text-center">Follow your brands</h2>
        <div class="navigation small-centered">
            <form method="get">
                <ul>

                    <li class="active">
                        <a href="{$baseurl}">Showroom</a>
                        <div class="box-filter scrollslider ">
                            <div class="search-filter">
                                <input type="text" placeholder="search" />
                            </div>
                            <div class="scroll-bar mCustomScrollbar" data-mcs-theme="dark-3">
                                <ul class="filters">
                                    {foreach $categories as $category}
                                        <li><a href="?cat={$category->cat_id}"> {$category->cat_name} </a></li>
                                    {/foreach}
                                </ul>
                            </div>
                        </div>

                    </li>

                    <li><a href="{$baseurl}new">All new</a> </li>
                    <li><a href="{$baseurl}favorites">Popular</a> </li>
                    <li><a href="{$baseurl}brands-to-follow">Follow Brands</a>

                        <div class="box-filter scrollslider ">
                            <div class="search-filter">
                                <input type="text" placeholder="search" />
                            </div>
                            <div class="scroll-bar mCustomScrollbar" data-mcs-theme="dark-3">
                                <ul class="filters">
                                    {foreach $brands as $brand}
                                        <li><a href="?brand={$brand->brand_id}"> {$brand->name} </a></li>
                                    {/foreach}
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li><a href="/?sale=1">On sale</a></li>

                </ul>
            </form>
        </div>
    </div>




    <div class="row products">
        <div id="container" class="large-12 collapse right">


            <div class="markenbox">
                <ul>
                    {foreach $trendbrands as $trendbrand}
                        {if $trendbrand->products|@count gte 4}
                        <li>
                            <div class="title-brand">
                                <span class="brand"><a href="?brand={$trendbrand->id}" >{$trendbrand->name}</a> </span>

                                <label for="{$trendbrand->id}">
                                    <input type="checkbox" class="brand" value="{$trendbrand->id}" id="{$trendbrand->id}" name="brand">
                                    <input type="hidden" class="user" value="{$userId}" name="userId">

                                    <button href="#" title="" class=" button follow btn-follow">Follow</button>

                                </label>

                            </div>
                            {foreach $trendbrand->products as $product }
                                <div class="itembox">
                                    <div class="item">
                                        <img data-original="{$product->product_image_small}" class="lazy" src="{$baseurl}assets/img/spinner.gif" />
                                    </div>
                                </div>
                            {/foreach}

                        </li>
                        {/if}
                    {/foreach}
                </ul>

            </div>





        </div>

    </div>

    <script type="text/javascript">
        //initiating jQuery
        jQuery(function($) {
            $(document).ready( function() {
                //enabling stickUp on the '.navbar-wrapper' class
                $('.navigation').stickUp();
            });
        });

    </script>
{/block}