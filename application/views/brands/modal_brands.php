<!-- Reveal Modals begin -->
<div id="firstModal" class="reveal-modal " data-reveal>
<h3 class="text-center">Select at least 5 designers you want to follow</h3>
<ul class="example-orbit" data-orbit>
<li>

    <div class="brands-collection large-3 columns">

        <img src="http://www.flaconi.de/media/images/promoBrands/131029-top-marken_yves-saint-laurent-01.jpg">

        <div class="row hide-for-small-only">
            <div class="large-12 small-6 columns">
                <img src="http://placehold.it/250x250&amp;text=Thumbnail">
            </div>
        </div>
        <div class="row">
            <div class="large-3 small-3 columns">
                <img src="http://i2.ztat.net/selector/FR/62/1C/02/OG/00/FR621C02O-G00@5.2.jpg">
            </div>
            <div class="large-3 small-3 columns">
                <img src="http://i2.ztat.net/selector/TF/12/1C/03/GQ/00/TF121C03G-Q00@2.1.jpg">
            </div>
            <div class="large-3 small-3 columns">
                <img src="http://i1.ztat.net/selector/GS/12/1A/09/QK/00/GS121A09Q-K00@5.1.jpg">
            </div>
            <div class="large-3 small-3 columns">
                <img src="http://i1.ztat.net/selector/DE/12/1H/02/5Q/11/DE121H025-Q11@6.jpg">
            </div>
        </div>

        <a class="large button expand [tiny small large]" href="#">Follow</a>
    </div>
    <div class="brands-collection large-3 columns">

        <img src="http://www.flaconi.de/media/images/promoBrands/131029-top-marken_yves-saint-laurent-01.jpg">

        <div class="row hide-for-small-only">
            <div class="large-12 small-6 columns">
                <img src="http://placehold.it/250x250&amp;text=Thumbnail">
            </div>
        </div>
        <div class="row">
            <div class="large-3 small-3 columns">
                <img src="http://placehold.it/250x250&amp;text=Thumbnail">
            </div>
            <div class="large-3 small-3 columns">
                <img src="http://placehold.it/250x250&amp;text=Thumbnail">
            </div>
            <div class="large-3 small-3 columns">
                <img src="http://placehold.it/250x250&amp;text=Thumbnail">
            </div>
            <div class="large-3 small-3 columns">
                <img src="http://placehold.it/250x250&amp;text=Thumbnail">
            </div>
        </div>

        <a class="large button expand [tiny small large]" href="#">Follow</a>
    </div>
    <div class="brands-collection large-3 columns">

        <img src="http://www.flaconi.de/media/images/promoBrands/131029-top-marken_chanel-01.jpg">

        <div class="row hide-for-small-only">
            <div class="large-12 small-6 columns">
                <img src="http://placehold.it/250x250&amp;text=Thumbnail">
            </div>
        </div>
        <div class="row">
            <div class="large-3 small-3 columns">
                <img src="http://placehold.it/250x250&amp;text=Thumbnail">
            </div>
            <div class="large-3 small-3 columns">
                <img src="http://placehold.it/250x250&amp;text=Thumbnail">
            </div>
            <div class="large-3 small-3 columns">
                <img src="http://placehold.it/250x250&amp;text=Thumbnail">
            </div>
            <div class="large-3 small-3 columns">
                <img src="http://placehold.it/250x250&amp;text=Thumbnail">
            </div>
        </div>

        <a class="large button expand [tiny small large]" href="#">Follow</a>
    </div>
    <div class="brands-collection large-3 columns">

        <img src="http://www.flaconi.de/media/images/promoBrands/131029-top-marken_yves-saint-laurent-01.jpg">

        <div class="row hide-for-small-only">
            <div class="large-12 small-6 columns">
                <img src="http://placehold.it/250x250&amp;text=Thumbnail">
            </div>
        </div>
        <div class="row">
            <div class="large-3 small-3 columns">
                <img src="http://placehold.it/250x250&amp;text=Thumbnail">
            </div>
            <div class="large-3 small-3 columns">
                <img src="http://placehold.it/250x250&amp;text=Thumbnail">
            </div>
            <div class="large-3 small-3 columns">
                <img src="http://placehold.it/250x250&amp;text=Thumbnail">
            </div>
            <div class="large-3 small-3 columns">
                <img src="http://placehold.it/250x250&amp;text=Thumbnail">
            </div>
        </div>

        <a class="large button expand [tiny small large]" href="#">Follow</a>
    </div>

</li>
<li>
    <div class="brands-collection large-3 columns">

        <img src="http://www.flaconi.de/media/images/promoBrands/131029-top-marken_yves-saint-laurent-01.jpg">

        <div class="row hide-for-small-only">
            <div class="large-12 small-6 columns">
                <img src="http://placehold.it/250x250&amp;text=Thumbnail">
            </div>
        </div>
        <div class="row">
            <div class="large-3 small-3 columns">
                <img src="http://placehold.it/250x250&amp;text=Thumbnail">
            </div>
            <div class="large-3 small-3 columns">
                <img src="http://placehold.it/250x250&amp;text=Thumbnail">
            </div>
            <div class="large-3 small-3 columns">
                <img src="http://placehold.it/250x250&amp;text=Thumbnail">
            </div>
            <div class="large-3 small-3 columns">
                <img src="http://placehold.it/250x250&amp;text=Thumbnail">
            </div>
        </div>

        <a class="large button expand [tiny small large]" href="#">Follow</a>
    </div>
    <div class="brands-collection large-3 columns">

        <img src="http://www.flaconi.de/media/images/promoBrands/131029-top-marken_yves-saint-laurent-01.jpg">

        <div class="row hide-for-small-only">
            <div class="large-12 small-6 columns">
                <img src="http://placehold.it/250x250&amp;text=Thumbnail">
            </div>
        </div>
        <div class="row">
            <div class="large-3 small-3 columns">
                <img src="http://placehold.it/250x250&amp;text=Thumbnail">
            </div>
            <div class="large-3 small-3 columns">
                <img src="http://placehold.it/250x250&amp;text=Thumbnail">
            </div>
            <div class="large-3 small-3 columns">
                <img src="http://placehold.it/250x250&amp;text=Thumbnail">
            </div>
            <div class="large-3 small-3 columns">
                <img src="http://placehold.it/250x250&amp;text=Thumbnail">
            </div>
        </div>

        <a class="large button expand [tiny small large]" href="#">Follow</a>
    </div>
    <div class="brands-collection large-3 columns">

        <img src="http://www.flaconi.de/media/images/promoBrands/131029-top-marken_yves-saint-laurent-01.jpg">

        <div class="row hide-for-small-only">
            <div class="large-12 small-6 columns">
                <img src="http://placehold.it/250x250&amp;text=Thumbnail">
            </div>
        </div>
        <div class="row">
            <div class="large-3 small-3 columns">
                <img src="http://placehold.it/250x250&amp;text=Thumbnail">
            </div>
            <div class="large-3 small-3 columns">
                <img src="http://placehold.it/250x250&amp;text=Thumbnail">
            </div>
            <div class="large-3 small-3 columns">
                <img src="http://placehold.it/250x250&amp;text=Thumbnail">
            </div>
            <div class="large-3 small-3 columns">
                <img src="http://placehold.it/250x250&amp;text=Thumbnail">
            </div>
        </div>

        <a class="large button expand [tiny small large]" href="#">Follow</a>
    </div>
    <div class="brands-collection large-3 columns">

        <img src="http://www.flaconi.de/media/images/promoBrands/131029-top-marken_yves-saint-laurent-01.jpg">

        <div class="row hide-for-small-only">
            <div class="large-12 small-6 columns">
                <img src="http://placehold.it/250x250&amp;text=Thumbnail">
            </div>
        </div>
        <div class="row">
            <div class="large-3 small-3 columns">
                <img src="http://placehold.it/250x250&amp;text=Thumbnail">
            </div>
            <div class="large-3 small-3 columns">
                <img src="http://placehold.it/250x250&amp;text=Thumbnail">
            </div>
            <div class="large-3 small-3 columns">
                <img src="http://placehold.it/250x250&amp;text=Thumbnail">
            </div>
            <div class="large-3 small-3 columns">
                <img src="http://placehold.it/250x250&amp;text=Thumbnail">
            </div>
        </div>

        <a class="large button expand [tiny small large]" href="#">Follow</a>
    </div>
</li>

</ul>
</div>