<?php
/**
 * Created by PhpStorm.
 * User: luis.gamero
 * Date: 27.07.14
 * Time: 14:51
 *
 * gender == 1 is women
 * gender == 0 is men
 */
if($gender == null):
    //redirect(base_url(), 301);
endif;

?>
<div class="row  quiz">

    <h3 class="text-center">1. Welcher Stil beschreibt Dich am Besten?</h3>

    <div class="row">

        <!--<ul id="status" class="center  ">
            <li class="active"><strong>1.</strong>Wähle Deinen Style</li>
            <li><strong>2.</strong>Wähle Deine Marken</li>
            <li><strong>3.</strong>Erstelle Dein Konto</li>
        </ul>-->

        <div class="progress success ">
            <span class="percent">1%</span>
            <span style="width: 2%" class="meter"></span>
        </div>

        <?php if($gender==1) : ?>
        <div class="stylebox">
            <h6 class="text-center">Trendy</h6>
            <label for="trendy">
                <input name="style" value="2" id="trendy" type="radio" class="next" >
                <img
                    src="<?php echo base_url();?>/assets/img/styles/hippy-chic.jpg">
            </label>
            <p>
                Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
            </p>

        </div>
        <div class="stylebox">
            <h6 class="text-center">Romantic</h6>
            <label for="Classic">
                <input name="style" value="5" id="Classic" type="radio" class="next">
                <img
                    src="<?php echo base_url();?>/assets/img/styles/classic-look.jpg" >
            </label>
            <p>
                Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
            </p>

        </div>
        <div class="stylebox">
            <h6 class="text-center">Casual</h6>
            <label for="romantic">
                <input name="style" value="3" id="romantic" type="radio" class="next">
                <img
                    src="<?php echo base_url();?>/assets/img/styles/sexy-glamour.jpg" >
            </label>
            <p>
                Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
            </p>

        </div>

        <div class="stylebox">
            <h6 class="text-center">Classic </h6>
            <label for="casual">
                <input name="style" value="1" id="casual" type="radio" class="left next">
                <img
                    src="<?php echo base_url();?>/assets/img/styles/minimalistic.jpg" >
            </label>
            <p>
                Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
            </p>

        </div>

        <div class="stylebox">
            <h6 class="text-center">Glamorous </h6>
            <label for="casual">
                <input name="style" value="4" id="casual" type="radio" class="left next">
                <img
                    src="<?php echo base_url();?>/assets/img/styles/minimalistic.jpg" >
            </label>
            <p>
                Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
            </p>

        </div>

        <?php else: ?>

            <div class="stylebox">
                <h6 class="text-center">Casual</h6>
                <label for="trendy">
                    <input name="style" value="1" id="trendy" type="radio" class="next" >
                    <img
                        src="<?php echo base_url();?>/assets/img/models/man-casual.jpg">
                </label>
                <p>
                    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
                </p>

            </div>
            <div class="stylebox">
                <h6 class="text-center">Classic</h6>
                <label for="Classic">
                    <input name="style" value="2" id="Classic" type="radio" class="next">
                    <input name="style" value="0" id="Classic" type="radio" class="next">
                    <img
                        src="<?php echo base_url();?>/assets/img/models/man-classic.jpg" >
                </label>
                <p>
                    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
                </p>

            </div>
            <div class="stylebox">
                <h6 class="text-center">Streetstyle</h6>
                <label for="romantic">
                    <input name="style" value="3" id="romantic" type="radio" class="next">
                    <img
                        src="<?php echo base_url();?>/assets/img/models/man-sporty.jpg" >
                </label>
                <p>
                    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
                </p>

            </div>

            <div class="stylebox">
                <h6 class="text-center">Sport look </h6>
                <label for="casual">
                    <input name="style" value="4" id="casual" type="radio" class="left next">
                    <img
                        src="<?php echo base_url();?>/assets/img/models/man-trendy.jpg" >
                </label>
                <p>
                    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
                </p>

            </div>

            <div class="stylebox">
                <h6 class="text-center">Trendy </h6>
                <label for="casual">
                    <input name="style" value="5" id="casual" type="radio" class="left next">
                    <img
                        src="<?php echo base_url();?>/assets/img/models/man-trendy.jpg" >
                </label>
                <p>
                    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
                </p>

            </div>


        <?php endif; ?>


   </div>
    <div class="nav-buttons">

        <div class="left">

        </div>
        <div class="right">
            <button type="button" class="next right" id="styleselect">Vor &raquo;</button>
        </div>

    </div>

</div>


<script>


 /*   jQuery("#styleselect").on('click',function(event ){
        var value= jQuery('input[name=style]:checked').val();
        var user_temp_id = Math.floor(Math.random() * 10);
        var user_data = user_temp_id + '|' + value;
        jQuery.cookie("user_data", [user_temp_id, value]);
    });
*/
 jQuery('.stylebox').on('click',function(event ){
        event.preventDefault();
        var $this = jQuery(this);
        var value= $this.find('input[name=style]').val();

        var postData = {
            'style' : value,
            'gender' : <?php echo $gender ?>
        };
        if(postData != ""){
            jQuery.ajax({
                type: "POST",
                url: "http://localhost/stylistr/user/user/createTempUser",
                data: postData,
                success: function(data){
                    location.href = "http://localhost/stylistr/register/quiz/steptwo"
                },
                error: function(){
                    alert("Fail")
                }
            });
        } else {
            alert("Insert Amount.");
        }
    });

</script>
