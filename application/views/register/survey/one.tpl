{extends file="layout.tpl"}
{block name="content"}
    <div class="row  quiz  reveal-modal medium" data-reveal>
    <h3 class="text-center">1. Welcher Stil beschreibt Dich am Besten?</h3>

    <!--<ul id="status" class="center  ">
        <li class="active"><strong>1.</strong>Wähle Deinen Style</li>
        <li><strong>2.</strong>Wähle Deine Marken</li>
        <li><strong>3.</strong>Erstelle Dein Konto</li>

        <div class="progress success ">
        <span class="percent">1%</span>
        <span style="width: 2%" class="meter"></span>
    </div>
    </ul>-->


    <div class="quiz-content">


    {if $gender eq 1 }

        {foreach $styles as $style}
            <div class="stylebox">
                <h6 class="text-center">{$style->style_name}</h6>
                <label for="{$style->style_name}">
                    <input name="style" value="{$style->id_style}" id="{$style->style_name}" type="checkbox" >
                    <img
                            src="{$baseurl}/assets/img/styles/{$style->style_image}">
                </label>
            </div>


        {/foreach}


    {/if}

    </div>

    <div class="nav-buttons">

        <div class="left">

        </div>
        <div class="right">
            <button type="button" class="next right" id="styleselect">Vor &raquo;</button>
        </div>


    </div>


</div>

    <div class="row products">

        {foreach $products as $product}
            <div class="box">
                <div class="love-box">
                    <button class="btn luv-it" data="{$product->id_product}">LUV IT</button>
                    <br/>
                    <a href="{base_url()}redirect/?id={$product->id_product}" target="_blank">
                        <button class="btn buy-it">BUY IT</button>
                    </a>
                </div>

                <div class="image-container" style="overflow: hidden">
                    <a href="{base_url()}redirect/?id={$product->id_product}" target="_blank">
                        <img class="lazy" data-original="{$product->product_image_medium}"
                             src="{base_url()}assets/img/spinner.gif">
                    </a>
                </div>

                <div class="panel">
                    <div class="product-brand">{$product->name}</div>
                    <div class="product-name"><a href="{$product->id_product}"
                                                 target="_blank">{$product->product_name|truncate:35}</a></div>
                    <div class="product-price">
                        {if ($product->product_old_price > 0)}
                            <span class="old-price">{$product->product_old_price}€</span>
                            Now: {$product->product_price} €
                        {else}
                            {$product->product_price} €
                        {/if}
                    </div>

                    {if isset($session)}
                        {if $session["username"] eq 'luardo123'}
                            <a class="luv" value="{$product->id_product}">Highlight</a>
                            |
                            <a class="delete" value="{$product->id_product}">Delete</a>
                        {/if}
                    {/if}

                </div>

            </div>
        {/foreach}

    </div>
    <script>

        jQuery(document).ready(function () {

            jQuery('.reveal-modal').foundation('reveal', 'open');
            jQuery(document).on('opened', '[data-reveal]', function () {
                jQuery(".quiz-content").mCustomScrollbar();
            });

        });

        jQuery('.next').on('click', function (event) {
            event.preventDefault();
            var $this = jQuery(this);
            var value = $this.find('input[name=style]').val();

            var myCheckboxes = new Array();
            jQuery("input:checked").each(function () {
                myCheckboxes.push(jQuery(this).val());
            });


            var postData = {
                'style': myCheckboxes,
                'gender': {$gender}
            };

            if (postData != "") {
                jQuery.ajax({
                    type: "POST",
                    url: "{$baseurl}register/quiz/createTempUser",
                    data: postData,
                    success: function (data) {
                        location.href = "{$baseurl}register/quiz/steptwo"
                    },
                    error: function () {
                        alert("Fail")
                    }
                });
            } else {
                alert("Insert Amount.");
            }
        });

    </script>
{/block}