{extends file="layout.tpl"}
{block name="content"}
    <div class="row    reveal-modal medium" data-reveal>
        <div class="row quiz">
            <h3 class="text-center">Welche der Marken findest Du am Besten?</h3>

            <!--<ul id="status" class="center  ">
                <li class="active"><strong>1.</strong>Wähle Deinen Style</li>
                <li><strong>2.</strong>Wähle Deine Marken</li>
                <li><strong>3.</strong>Erstelle Dein Konto</li>
            </ul>-->

            <div  class="quiz-content">

                <div class="markenbox">
                    <ul>


                        {foreach $products as $brand}
                            <li>
                                <div class="title-brand">
                                    <span class="brand">{$brand@key}</span>
                                    <label for="{$brand[0][0]->product_brand_id}">
                                        <input name="brand" id="{$brand[0][0]->product_brand_id}"
                                               value="{$brand[0][0]->product_brand_id}" type="checkbox">

                                        <button class=" button follow " title="" href="#">Follow</button>

                                    </label>
                                </div>

                                {foreach $brand[0] as $product }
                                    <div class="itembox">
                                        <div class="item">
                                            <img data-original="{$product->product_image_small}" class="thumbs"
                                                 src="{$baseurl}assets/img/spinner.gif"/>
                                        </div>
                                    </div>
                                {/foreach}


                            </li>
                        {/foreach}


                    </ul>


                </div>

            </div>




        </div>
        <div class="nav-buttons">
            <div class="left">
                <button type="button" class="next left">&laquo; Zurück</button>
            </div>
            <div class="right">
                <button type="button" class="next right">Vor &raquo;</button>
            </div>

        </div>
    </div>
    <script>
        jQuery(document).ready(function () {


            jQuery('.reveal-modal').foundation('reveal', 'open');

            jQuery(document).on('opened', '[data-reveal]', function () {
                jQuery(".quiz-content").mCustomScrollbar();

                jQuery("img.thumbs").lazyload({
                    container: jQuery(".markenbox")
                });
            });

        });

        jQuery('label').on('click', function (event) {

            if (jQuery(this).find('input').prop('checked')) {
                jQuery(this).find('input').prop("checked", false);
                jQuery(this).find('.follow').removeClass('success');
                jQuery(this).find('.follow').text("Follow");
                // jQuery(this).closest('li').css("backgroundColor", "#efefef");
            }
            else {
                jQuery(this).find('.follow').addClass('success');
                jQuery(this).find('.follow').text("Following");
                jQuery(this).find('input').prop("checked", true);
                // jQuery(this).closest('li').css("backgroundColor", "#F6CEF5");
            }

        });

        jQuery('.next').on('click', function (event) {
            event.preventDefault();
            var $this = jQuery(this);
            var value = $this.find('input[name=brand]').val();
            var myCheckboxes = new Array();
            jQuery("input:checked").each(function () {
                myCheckboxes.push(jQuery(this).val());
            });


            var postData = { 'brands': myCheckboxes };
            if (postData != "") {
                jQuery.ajax({
                    type: "POST",
                    url: "{$baseurl}register/quiz/editBrands",
                    data: postData,
                    success: function (data) {
                        location.href = "{$baseurl}register/quiz/stepthree"
                    },
                    error: function () {
                        alert("Fail")
                    }
                });
            } else {
                alert("Insert Amount.");
            }
        });
    </script>
{/block}