{extends file="layout.tpl"}
{block name="content"}
    <div class="row  quiz  reveal-modal medium" data-reveal>


        <h3 class="text-center">We are almost ready! </h3>

        <div class="row">

            <!--<ul id="status" class="center  ">
                <li class="active"><strong>1.</strong>Wähle Deinen Style</li>
                <li><strong>2.</strong>Wähle Deine Marken</li>
                <li><strong>3.</strong>Erstelle Dein Konto</li>
            </ul>-->


            <div class="large-6 small-12 columns new-member">
                <h3 >Erstelle Dein Konto</h3>
                <div class="row small-centered">
                    <form action="{$baseurl}register/quiz/createGuestUser" method="post">

                    <ul>
                        <!-- email -->

                        {if isset($user_id)}
                        <input type="hidden" value="{$user_id}" name="user[user_id]" >
                        {/if}
                        <li class="required">
                            <label>
                                <input type="text" class="text" name="user[email]" value="" placeholder="E-Mail Adresse"/ required/>
                                <em>Your password will be sent to this address. Your
                                    address will not made public.</em>
                            </label>
                        </li>

                        <!-- username -->
                        <li>
                            <label>
                                <input type="text" class="text" name="user[username]" required placeholder="Wähle Deinen Usernamen "/>
                                <em>Your preferred username to be used when logging in.</em>
                            </label>
                        </li>

                        <!-- password -->
                        <li class="required double">

                            <label>
                                <input type="password" class="text" name="user[password]" id="password" required placeholder="Wähle ein Passwort"/>
                                <em>Must be at least 8 characters long.</em>
                            </label>

                            <label>

                                <input type="password" class="text" data-equalto="password" name="confirmpassword" required placeholder="Wiederhole das Passwort "/>
                            </label>
                        </li>

                        <input type="hidden" name="user[gender]" value="1" />


                    </ul>
                        <hr>



                        <button type="submit" class="success submit">
                            Anmelden &raquo;
                        </button>


                </div>
                </form>



            </div>

            <div class="large-6 small-12 columns benefits">

                <h3>Benefits of been a member</h3>
                <ul>
                    <li>Explore the best products from your favorite brands</li>
                    <li>Get weekly recommendations and outfits picked just for you</li>
                    <li>Be the first to know when your favorite items go on sale!</li>
                </ul>

                <a class="button right" href="{$baseurl}catalog/showroom">
                    Go to my showrrom &raquo;
                </a>





            </div>


        </div>
    </div>

    <div class="row products">

        {foreach $products as $product}
            <div class="box">
                <div class="love-box">
                    <button class="btn luv-it" data="{$product->id_product}">LUV IT</button>
                    <br/>
                    <a href="{base_url()}redirect/?id={$product->id_product}" target="_blank">
                        <button class="btn buy-it">BUY IT</button>
                    </a>
                </div>

                <div class="image-container" style="overflow: hidden">
                    <a href="{base_url()}redirect/?id={$product->id_product}" target="_blank">
                        <img class="lazy" data-original="{$product->product_image_medium}"
                             src="{base_url()}assets/img/spinner.gif">
                    </a>
                </div>

                <div class="panel">
                    <div class="product-brand">{$product->name}</div>
                    <div class="product-name"><a href="{$product->id_product}"
                                                 target="_blank">{$product->product_name|truncate:35}</a></div>
                    <div class="product-price">
                        {if ($product->product_old_price > 0)}
                            <span class="old-price">{$product->product_old_price}€</span>
                            Now: {$product->product_price} €
                        {else}
                            {$product->product_price} €
                        {/if}
                    </div>

                    {if isset($session)}
                        {if $session["username"] eq 'luardo123'}
                            <a class="luv" value="{$product->id_product}">Highlight</a>
                            |
                            <a class="delete" value="{$product->id_product}">Delete</a>
                        {/if}
                    {/if}

                </div>

            </div>
        {/foreach}

    </div>

    <script>
        jQuery(document).ready(function(){
                jQuery('.reveal-modal').foundation('reveal', 'open');

        });
    </script>
{/block}