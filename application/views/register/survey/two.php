<?php
/**
 * Created by PhpStorm.
 * User: luis.gamero
 * Date: 27.07.14
 * Time: 16:15
 */

if(!$this->session->userdata('user_data')):
    redirect(base_url(), 301);
endif;
?>
<div class="row  quiz">


    <h3 class="text-center">Welche der Marken findest Du am Besten?</h3>

    <div class="row">

        <!--<ul id="status" class="center  ">
            <li class="active"><strong>1.</strong>Wähle Deinen Style</li>
            <li><strong>2.</strong>Wähle Deine Marken</li>
            <li><strong>3.</strong>Erstelle Dein Konto</li>
        </ul>-->

        <div class="progress success ">
            <span class="percent">8%</span>
            <span style="width: 10%" class="meter"></span>
        </div>

        <div class="markenbox">
            <ul>

                <?php foreach ($products as $brand => $productArray):
                    $brandId = $productArray[0][0]->product_brand_id;
                    ?>
                <li>
                    <div class="title-brand">
                        <span class="brand"><?php echo $brand ?></span>
                        <label for="<?php echo $brandId ?>">
                            <input name="brand" id="<?php echo $brandId ?>" value="<?php echo $brandId ?>" type="checkbox" >

                            <button class=" button follow " title="" href="#">Follow</button>

                        </label>
                    </div>
                    <div class="itembox">

                    <?php foreach ($productArray as $products): ?>

                        <?php $i=0; foreach ($products as $product): ?>
                            <div class="item">
                                <img data-original="<?php echo $product->product_image_small ?>" class="thumbs" src="<?php echo base_url();?>assets/img/spinner.gif" />
                            </div>

                            <?php if($i>=7): break; endif; ?>

                        <?php $i++; endforeach; ?>
                    </div>
                    <?php endforeach; ?>

                </li>
                <?php endforeach; ?>




            </ul>


        </div>

        <div class="nav-buttons">

            <div class="left">
                <button type="button" class="next left">&laquo; Zurück</button>
            </div>
            <div class="right">
                <button type="button" class="next right">Vor &raquo;</button>
            </div>

        </div>



    </div>
</div>

<script>
jQuery(document).ready(function(){
    jQuery(function() {
        jQuery("img.thumbs").lazyload({
            container: jQuery(".markenbox")
        });

    });
});

jQuery('label').on('click',function(event ){
    jQuery(this).find('.follow').addClass('success');
    jQuery(this).find('.follow').text("Following");
    jQuery(this).find('input').prop( "checked", true );
});

jQuery('.next').on('click',function(event ){
    event.preventDefault();
    var $this = jQuery(this);
    var value= $this.find('input[name=brand]').val();
    var myCheckboxes = new Array();
    jQuery("input:checked").each(function() {
        myCheckboxes.push(jQuery(this).val());
    });



    var postData = { 'brands' : myCheckboxes };
    if(postData != ""){
        jQuery.ajax({
            type: "POST",
            url: "http://localhost/stylistr/user/user/editBrands",
            data: postData,
            success: function(data){
               location.href = "http://localhost/stylistr/register/quiz/stepthree"
            },
            error: function(){
                alert("Fail")
            }
        });
    } else {
        alert("Insert Amount.");
    }
});
</script>