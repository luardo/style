<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.tools.min.js"></script>
<div class="clearfix">
</div>
<?php
if (isset($gender) || $gender==''):
?>
<form action="<?php echo base_url();?>register/quiz/register" method="post">
<div class="row wizard-wrapper" style="background: white">

    <div class="large-12 columns " id="wizard">

        <br>

        <h2 class="text-center">STYLE PROFILE</h2>
        <br>
            <ul id="status" class="center  small-9">
                <li class="active"><strong>1.</strong>Wähle Deinen Style</li>
                <li><strong>2.</strong>Wähle Deine Marken</li>
                <li><strong>3.</strong>Erstelle Dein Konto</li>
            </ul>

        <div class="items">

            <div class="large-12 page ">

                <h3 class="text-center">1. Welcher Stil beschreibt Dich am Besten?</h3>
                <hr>
                <?php
                //Validates if fashion for woman or men was selected
                if( $gender == 1 ): ?>
                <div class="large-3 medium-3 small-6 columns">
                    <label for="trendy">
                        <input name="style" value="0" id="trendy" type="radio" class="next" >
                        <img
                            src="<?php echo base_url();?>/assets/img/styles/hippy-chic.jpg">
                    </label>
                    <h6 class="text-center">City Chic</h6>
                </div>
                <div class="large-3 medium-3 small-6 columns">
                    <label for="Classic">
                        <input name="style" value="0" id="Classic" type="radio" class="next">
                        <img
                        src="<?php echo base_url();?>/assets/img/styles/classic-look.jpg" >
                    </label>
                    <h6 class="text-center">Classic</h6>
                </div>
                <div class="large-3 medium-3 small-6 columns">
                    <label for="romantic">
                        <input name="style" value="0" id="romantic" type="radio" class="next">
                        <img
                            src="<?php echo base_url();?>/assets/img/styles/sexy-glamour.jpg" >
                    </label>
                    <h6 class="text-center">Sexy casual</h6>
                </div>

                <div class="large-3 medium-3 small-6 columns">
                    <label for="casual">
                        <input name="style" value="0" id="casual" type="radio" class="left next">
                        <img
                            src="<?php echo base_url();?>/assets/img/styles/minimalistic.jpg" >
                    </label>
                    <h6 class="text-center">Minimalist </h6>
                </div>


                <?php else: //options if its a man registration ?>
                    <div class="large-3 medium-3 small-6 columns">
                        <label for="trendy">
                            <input name="style" value="0" id="trendy" type="radio" class="next" >
                            <img
                                src="<?php echo base_url();?>/assets/img/models/man-casual.jpg">
                        </label>
                        <h6 class="text-center">Casual</h6>
                    </div>
                    <div class="large-3 medium-3 small-6 columns">
                        <label for="Classic">
                            <input name="style" value="0" id="Classic" type="radio" class="next">
                            <img
                                src="<?php echo base_url();?>/assets/img/models/man-classic.jpg" >
                        </label>
                        <h6 class="text-center">Classic</h6>
                    </div>
                    <div class="large-3 medium-3 small-6 columns">
                        <label for="romantic">
                            <input name="style" value="0" id="romantic" type="radio" class="next">
                            <img
                                src="<?php echo base_url();?>/assets/img/models/man-sporty.jpg" >
                        </label>
                        <h6 class="text-center">Sport look</h6>
                    </div>

                    <div class="large-3 medium-3 small-6 columns">
                        <label for="casual">
                            <input name="style" value="0" id="casual" type="radio" class="left next">
                            <img
                                src="<?php echo base_url();?>/assets/img/models/man-trendy.jpg" >
                        </label>
                        <h6 class="text-center">Trendy </h6>
                    </div>


                <?php endif; ?>

                <li class="clearfix">
                    <button type="button" class="next right">Vor &raquo;</button>
                </li>


            </div>

            <div class="large-12 page ">
                <h4 class="text-center">2. Welche der Marken findest Du am Besten?</h4>
                <hr>
                <div class="brands-container" id="brands-container" style="height: 300px; overflow: auto">

                <ul class="inline-list">
                <?php foreach($brands as $brand):  ?>
                    <li>
                        <div style="height: 40px; vertical-align: middle">
                            <label for="<?php echo $brand->name; ?>">
                                <input type="checkbox" value="<?php echo $brand->id ?>" id="<?php echo $brand->name; ?>"
                                       name="brand[]"/>

                                <div class="<?php echo strtolower(preg_replace('/\s+/', '', $brand->name)); ?>"></div>
                            </label>
                        </div>
                    </li>


                <?php endforeach ?>
                </ul>
                </div>


                <li class="clearfix">
                    <button type="button" class="prev" style="float:left">
                        &laquo; Zurück
                    </button>
                    <button type="button" class="next right">
                        Vor &raquo;
                    </button>
                </li>

            </div>

            <div class="large-12 page ">
                <h4 class="text-center">3. Erstelle Dein Konto</h4>
                <hr>
                <div class="large-6 columns small-centered">

                    <ul>
                        <!-- email -->
                        <li class="required">
                            <label>
                                <strong>1.</strong> E-Mail Adresse <span>*</span><br/>
                                <input type="text" class="text" name="user[email]" value="<?php if(isset($email)): echo $email; endif ?>" required/>
                                <em>Your password will be sent to this address. Your
                                    address will not made public.</em>
                            </label>
                        </li>

                        <!-- username -->
                        <li>
                            <label>
                                <strong>2.</strong> Wähle Deinen Usernamen <br/>
                                <input type="text" class="text" name="user[username]" required/>
                                <em>Your preferred username to be used when logging in.</em>
                            </label>
                        </li>

                        <!-- password -->
                        <li class="required double">

                            <label>
                                <strong>3.</strong> Wähle ein Passwort <span>*</span><br/>
                                <input type="password" class="text" name="user[password]" id="password" required/>
                                <em>Must be at least 8 characters long.</em>
                            </label>

                            <label>
                                Wiederhole das Passwort <span>*</span><br/>
                                <input type="password" class="text" data-equalto="password" name="confirmpassword" required/>
                            </label>
                        </li>

                        <input type="hidden" name="user[gender]" value="<?php echo $gender ?>" />


                    </ul>

                    <li class="clearfix">
                        <button type="button" class="prev" style="float:left">
                            &laquo; Zurück
                        </button>
                        <button type="submit" class="right success submit">
                            Anmelden &raquo;
                        </button>
                    </li>


                </div>



            </div>

        </div>

    </div>

</div>

</form>


<!-- activate tabs with JavaScript -->
<script>
    $(function () {
        var root = $("#wizard").scrollable();
        var quiz = $("#wizard").expose({color: '#789'});
        // some variables that we need
        var api = root.scrollable(), drawer = $("#drawer");

        // validation logic is done inside the onBeforeSeek callback
        api.onBeforeSeek(function (event, i) {

            // we are going 1 step backwards so no need for validation
            if (api.getIndex() < i) {

                // 1. get current page
                var page = root.find(".page").eq(api.getIndex()),

                // 2. .. and all required fields inside the page
                    inputs = page.find(".required :input").removeClass("error"),

                // 3. .. which are empty
                    empty = inputs.filter(function () {
                        return $(this).val().replace(/\s*/g, '') == '';
                    });

                // if there are empty fields, then
                if (empty.length) {

                    // slide down the drawer
                    drawer.slideDown(function () {

                        // colored flash effect
                        drawer.css("backgroundColor", "#229");
                        setTimeout(function () {
                            drawer.css("backgroundColor", "#fff");
                        }, 1000);
                    });

                    // add a CSS class name "error" for empty & required fields
                    empty.addClass("error");

                    // cancel seeking of the scrollable by returning false
                    return false;

                    // everything is good
                } else {

                    // hide the drawer
                    drawer.slideUp();
                }

            }

            // update status bar
            $("#status li").removeClass("active").eq(i).addClass("active");

        });
        // enable exposing on the wizard
        $( document ).ready(function() {
            quiz.expose().load();
        });

        // if tab is pressed on the next button seek to next page
        root.find(".next").keydown(function (e) {
            if (e.keyCode == 9) {

                // seeks to next tab by executing our validation routine
                api.next();
                e.preventDefault();
            }
        });
    });
</script>

<?php
else:
    redirect(base_url(), 302);
endif; ?>
