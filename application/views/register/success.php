<?php
    $email = $this->input->get('token', TRUE);
    if(!isset($email) || is_null($email) || $email == ''):
        redirect(base_url(),  302);
    endif;
?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/jquery.flipcountdown.css" />
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.flipcountdown.js"></script>

<div class="row" style="margin-top: 25px">
    <h2>Your account</h2>
    <hr>
    <div class="large-10 columns  small-centered">
        <h4 class="text-center success">Your registration has been complete, thank you!</h4>
        <p  class="text-center">At the moment we are  building this great place and we still have many
            challenges to give you the best customer experience. We will let you know once we are ready.</p>
        <h1 class="text-center">We will launch soon... </h1>
        <table style="border:0px; padding: 0px; margin: 0 auto;" >
            <tr>
                <td style="width:110px;text-align:center; padding: 0px">Days</td>
                <td style="width:70px;text-align:center; padding: 0px">Hours</td>
                <td style="width:70px;text-align:center; padding: 0px">Minutes</td>
                <td style="width:90px;text-align:center; padding: 0px">Seconds</td>
            </tr>
            <tr>
                <td colspan="4"><span id="new_year"></span></td>
            </tr>
        </table>
        <br /><br />
        <!-- <h4 class="text-center">Your opinion is important for us</h4>
        <p class="text-center">If you have any ideas, comments, critic or suggestions of how could we improve your user experience, please
        don't hesitate on writing us. </p> -->

        <p class="text-center">

            <!-- A link to launch the Classic Widget -->
            <a href="javascript:void(0)" class="button" data-uv-trigger>Give us your feedback</a>
        </p>
    </div>
   <!-- <h1 class="text-center">The worlds top fashion in one place</h1>
    <h3 class="text-center">Follow the Brands and designers you love</h3>


    <br />


    <div class="large-6 medium-6 small-10 small-centered columns  ">
        <a class="large button hide-for-small"  data-reveal-id="firstModal" href="#">Follow your style</a>
        <a class="small button show-for-small" data-reveal-id="firstModal" href="#">Shop it now!</a>
    </div><hr>-->

    <div id="feedback-form" class="reveal-modal small text-center" data-reveal>
        <h5>Your feed back is very important for us.</h5>
        <br /><br />
        <form id="feedback">
            <input type="hidden" value="<?php echo $this->input->get('token', TRUE); ?>" name="email" />
            <div class="row">
                <div class="large-12 columns" id="feedback-content">

                        <textarea placeholder="Your comment: Max. 500 characters" rows="15" name="comment"></textarea>


                    <button type="submit" class="right">Send</button>
                </div>
            </div>
        </form>

        <script>
            // Include the UserVoice JavaScript SDK (only needed once on a page)
            UserVoice=window.UserVoice||[];(function(){var uv=document.createElement('script');uv.type='text/javascript';uv.async=true;uv.src='//widget.uservoice.com/fannbD4mWIOzZj2DSiBrg.js';var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(uv,s)})();

            //
            // UserVoice Javascript SDK developer documentation:
            // https://www.uservoice.com/o/javascript-sdk
            //

            // Set colors
            UserVoice.push(['set', {
                accent_color: '#448dd6',
                trigger_color: 'white',
                trigger_background_color: 'rgba(46, 49, 51, 0.6)'
            }]);

            // Identify the user and pass traits
            // To enable, replace sample data with actual user traits and uncomment the line
            UserVoice.push(['identify', {
                email:      '<?php echo $this->input->get('token', TRUE); ?>' // User’s email address
                //name:       'John Doe', // User’s real name
                //created_at: 1364406966, // Unix timestamp for the date the user signed up
                //id:         123, // Optional: Unique id of the user (if set, this should not change)
                //type:       'Owner', // Optional: segment your users by type
                //account: {
                //  id:           123, // Optional: associate multiple users with a single account
                //  name:         'Acme, Co.', // Account name
                //  created_at:   1364406966, // Unix timestamp for the date the account was created
                //  monthly_rate: 9.99, // Decimal; monthly rate of the account
                //  ltv:          1495.00, // Decimal; lifetime value of the account
                //  plan:         'Enhanced' // Plan name for the account
                //}
            }]);

            // Add default trigger to the bottom-right corner of the window:
            //UserVoice.push(['addTrigger', { mode: 'contact', trigger_position: 'bottom-right' }]);

            // Or, use your own custom trigger:
            //UserVoice.push(['addTrigger', '#id', { mode: 'contact' }]);

            // Autoprompt for Satisfaction and SmartVote (only displayed under certain conditions)
            UserVoice.push(['autoprompt', {}]);
        </script>


        <script type="text/javascript">
            jQuery(function($){
                var NY = Math.round((new Date('09/01/2014 00:00:01')).getTime()/1000);
                $('#new_year').flipcountdown({
                    size:"md",
                    tick:function(){
                        var nol = function(h){
                            return h>9?h:'0'+h;
                        }
                        var	range  	= NY-Math.round((new Date()).getTime()/1000),
                            secday = 86400, sechour = 3600,
                            days 	= parseInt(range/secday),
                            hours	= parseInt((range%secday)/sechour),
                            min		= parseInt(((range%secday)%sechour)/60),
                            sec		= ((range%secday)%sechour)%60;
                        return nol(days)+' '+nol(hours)+' '+nol(min)+' '+nol(sec);
                    }
                });
            });

            $(document).ready(function() {
                $("#feedback").submit(function() {
                    $.ajax({
                        type : 'POST',
                        url : "<?php echo base_url();?>feedback/feedbackpost",
                        async : false,
                        data : $(this).serialize(),
                        success : function(msg) {
                            $('#feedback-form').html('<h4>Your message has been sent!</h4> <p>Thank you for your support</p> <a href="<?php echo base_url();?>"><button>Back to home</button></a>');

                        }
                    });

                    return false;
                });
            });
        </script>

    </div>




</div>



