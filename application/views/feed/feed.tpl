{extends file="layout.tpl"}
{block name="content"}
    <div class="row">
        <table>
            <thead>
            <tr>
                <th>File</th>
                <th>Shop</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>

                {foreach $csvFiles as $csvFile}
                <tr>
                    <form method="get" id="{$csvFile['store']}-form">
                    <td>{$csvFile['file']}
                    <input type="hidden" name="csv" value="{$csvFile['file']}">
                    </td>
                    <td>{$csvFile['store']}
                        <input type="hidden" name="store" value="{$csvFile['store']}">
                    </td>
                    <td><a onclick="document.getElementById('{$csvFile['store']}-form').submit();">Import</a></td>
                    </form>
                </tr>
                {/foreach}

            </tbody>
        </table>

    </div>
{/block}