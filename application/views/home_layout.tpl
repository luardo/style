<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Latest fashion collections, season trends and designer fashion from all the world in one place - Styluv. Love it, style it</title>
    <link href='http://fonts.googleapis.com/css?family=Julius+Sans+One' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Hammersmith+One' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{$baseurl}/assets/css/foundation.css" />
    <link rel="stylesheet" href="{$baseurl}/assets/css/jquery.mCustomScrollbar.css" />
    <link rel="stylesheet" href="{$baseurl}/assets/css/style.css" />
    <link rel="stylesheet" href="{$baseurl}/assets/css/sprites.css" />
    <script src="{$baseurl}/assets/js/vendor/modernizr.js"></script>
    <script src="{$baseurl}/assets/js/jquery.min.js"></script>

    <script src="{$baseurl}/assets/js/vendor/jquery.cookie.js"></script>
    <script src="{$baseurl}/assets/js/vendor/jquery.lazyload.js"></script>
    <script src="{$baseurl}/assets/js/stickUp.min.js"></script>
    <meta name="verification" content="adbf4ef53034297f79b11d03ef6d4c6d" />



    <!-- KISSmetrics tracking snippet -->
    <script type="text/javascript">var _kmq = _kmq || [];
        var _kmk = _kmk || 'b296dc1a6b829846ea9162ba0ab78967bcaa28a6';
        function _kms(u){
            setTimeout(function(){
                var d = document, f = d.getElementsByTagName('script')[0],
                        s = d.createElement('script');
                s.type = 'text/javascript'; s.async = true; s.src = u;
                f.parentNode.insertBefore(s, f);
            }, 1);
        }
        _kms('//i.kissmetrics.com/i.js');
        _kms('//doug1izaerwt3.cloudfront.net/' + _kmk + '.1.js');
    </script>


</head>
<body>


<div class="contain-to-grid">
    <nav class="top-bar" data-topbar>


        <ul class="title-area">
            <li class="name">
                <a href="{$baseurl}"> <img src="{$baseurl}assets/img/styluv-logo.png" alt="Follow your favorite brands: Gucci, Chloe, Armani, Burbery and more at Styluv" /></a>
            </li>
        </ul>
        <section class="top-bar-section">

            <ul class="right">
                {ci helper='navigation' function='get_header_navigation'}
            </ul>
        </section>

    </nav>
</div>


<section id="home">
    <div class="large-4  medium-6 small-centered columns  homepage " style="background: #ffffff;">
        <h3>Finde Mode die Du liebst! </h3>
        <hr>
        <h6 class="text-center ">Ausgewählte Kollektionen und Trends nur für Dich</h6>
        <br/>
        <form action="{$baseurl}register/quiz/stepone" method="post">
            <div class="row">
                <div class="large-12 medium-12 small-12 small-centered columns">

                    <label>Ich suche nach: </label>
                    <br/>
                    <ul class="inline-list">
                        <li>
                            <input name="gender" value="1" id="women" type="radio"   checked="checked"  ><label for="women">Mode für Damen</label>
                        </li>
                        <li>
                            <input name="gender" value="0" id="men" type="radio" ><label for="men">Mode für Herren</label>
                        </li>
                    </ul>
                    <br/>


                </div>
            </div>
            <div class="large-12 medium-12 small-12 small-centered columns  ">
                <button class="button" type="submit">Los geht's</button>
            </div>
        </form>
    </div>


</section>

<div class="wrapper">

{block name="content"}{/block}



<footer class="row">
    <div class="large-12 columns">
        <hr>

        <div class="row">
            <div class="large-6 columns">
                <p></p>
            </div>

            <div class="large-6 columns">
                <ul class="inline-list right">
                    <li>
                        <a href="{$baseurl}contact">Kontakt</a>
                    </li>

                    <li>
                        <a href="{$baseurl}about">Über uns</a>
                    </li>

                    <li>
                        <a href="{$baseurl}impressum">Impressum</a>
                    </li>

                </ul>
            </div>
        </div>
    </div>
</footer>

</div>




</body>
</html>