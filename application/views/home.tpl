{extends file="landingpage.tpl"}
{block name="content"}


<div class="row brands">
    <div class="large-12">

    </div>
</div>



<div class="row homepage-howto">
    <h2 class="text-center">So Funktioniert's</h2>
    <div class="medium-4 columns ">
        <img src="{$baseurl}/assets/img/howitworks1_02.jpg" />
        <h3>Beschreibe Deinen Style</h3>
        <p>Wähle den Style aus, der am meisten zu Dir passt.</p>
    </div>
    <div class="medium-4 columns ">
        <img src="{$baseurl}/assets/img/howitworks1_03.jpg" />
        <h3>Folge Deinen Marken</h3>
        <p>Wähle deine Lieblingsmarken, Designer und Läden. </p>
    </div>
    <div class="medium-4 columns ">
        <img src="{$baseurl}/assets/img/howitworks1_04.jpg" />
        <h3>Dein persönlicher Showroom</h3>
        <p>Wir finden die neuesten Trends und Kollektionen, die Deinem Stil entsprechen. </p>
    </div>
    <!-- <h1 class="text-center">The worlds top fashion in one place</h1>
     <h3 class="text-center">Follow the Brands and designers you love</h3>


     <br />


     <div class="large-6 medium-6 small-10 small-centered columns  ">
         <a class="large button hide-for-small"  data-reveal-id="firstModal" href="#">Follow your style</a>
         <a class="small button show-for-small" data-reveal-id="firstModal" href="#">Shop it now!</a>
     </div><hr>-->
</div>

{/block}

