<div class="row products">
    <ul class="breadcrumbs">
        <li><a href="<?php echo base_url() ?>">Home</a></li>
        <li><a href="<?php echo base_url() ?>">Shop</a></li>
        <li class="current"><a href="#">Style Room</a></li>
    </ul>

    <h2 class="text-center"><?php //echo $username ?>My Boutique</h2>
    <h5 class="text-center">All new from <br />
        <?php
        foreach($brands as $brand):
            echo $brand->name." / ";
        endforeach;

        $j=1;
        $c=1;
        ?>
    </h5>
    <hr>
    <div class="pagination text-right">
        <?php echo $this->pagination->create_links(); ?>
    </div>

    <?php
    $looks = array();

    $looks = array(
        'https://farm3.staticflickr.com/2919/14830888502_53acc4b70a_o.jpg',
        'http://www.ohhcouture.com/wp-content/uploads/2014/08/IMG_8361.jpg',
        'http://imnext.se/victoriatornegren/wp-content/uploads/sites/14/2014/08/2.jpg',
        'http://imnext.se/victoriatornegren/wp-content/uploads/sites/14/2014/07/P7302211.jpg',
        'http://imnext.se/victoriatornegren/wp-content/uploads/sites/14/2014/07/P6251047-0012.jpg',

    );

    ?>
    <div id="container" class="large-9 columns">

        <div class="row">

            <?php $i=0; foreach($products as $product): ?>
                <div class="box">
                    <div class="image-container" style="overflow: hidden">
                        <a href="<?php echo $product->product_link; ?>" target="_blank" >
                            <img class="lazy" data-original="<?php echo $product->product_image_medium; ?>" src="<?php echo base_url();?>assets/img/spinner.gif">
                        </a>
                    </div>

                    <div class="panel">
                        <div class="product-brand"><?php echo $product->name; ?></div>
                        <div class="product-name"><a href="<?php echo $product->product_link; ?>" target="_blank"><?php echo $product->product_name; ?></a></div>
                        <div class="product-price"><?php echo $product->product_price; ?> €</div>
                    </div>

                </div>

            <?php if($i % 5 == 0):  ?>
                    <?php foreach($looks as $look): ?>
                        <?php if($j==$c): ?>
                        <div class="box">
                            <div class="image-container">
                                <img src="<?php echo $look ?>" width="350" />


                            </div>
                            <div class="panel">
                               <span class="look">Look der Woche</span>
                            </div>

                        </div>
                        <?php break; endif; ?>
                        <?php $j++; endforeach ?>
            <?php $c++; $j=1; endif; ?>





            <?php $i++; endforeach ?>

        </div>
    </div>
    <div class="large-3 columns">
        <h6>Welcome back  <?php /*echo $username */?></h6>
        <hr>
        <h5>Top brands</h5>
        <?php /*$this->load->view('brands/brands_loop'); */?>
        <!-- START of the zanox affiliate HTML code -->
        <!-- ( The HTML code may not be changed in the sense of faultless functionality! ) -->
        <a href="http://ad.zanox.com/ppc/?28670217C1398938150T"><img src="http://ad.zanox.com/ppv/?28670217C1398938150" align="bottom" width="300" height="250" border="0" hspace="1" alt="300x250_afbeelding_2"></a>
        <!-- ENDING of the zanox-affiliate HTML-Code -->
        <br />
        <!-- START of the zanox affiliate HTML code -->
        <!-- ( The HTML code may not be changed in the sense of faultless functionality! ) -->
        <a href="http://ad.zanox.com/ppc/?28670221C1256655206T"><img src="http://ad.zanox.com/ppv/?28670221C1256655206" align="bottom" width="300" height="250" border="0" hspace="1" alt="Banner Damen 300x250"></a>
        <!-- ENDING of the zanox-affiliate HTML-Code -->
        <br />
        <!-- START of the zanox affiliate HTML code -->
        <!-- ( The HTML code may not be changed in the sense of faultless functionality! ) -->
        <img src="http://ad.zanox.com/ppv/?28670223C999079394" align="bottom" width="1" height="1" border="0" hspace="1"><script language="javascript" src="http://ad.zanox.com/ppv/images/programs/flash_load/flash_loader_ng.js"></script><script type="text/javascript">zxFlash("http://static.vintywomen.com/banners/de/VWDE300x250.swf", "http://static.vintywomen.com/banners/de/VWDE300x250.jpg", 300, 250, "opaque", "http://ad.zanox.com/ppc/?28670223C999079394&ULP=", "XXX");</script><noscript><a href="http://ad.zanox.com/ppc/?28670223C999079394&ULP=" target="_blank"><img src="http://static.vintywomen.com/banners/de/VWDE300x250.jpg" border="0" width="300" height="250" alt="banners Deutsch Vintywomen Fallback Image"></a></noscript>
        <!-- ENDING of the zanox-affiliate HTML-Code -->
        <br />


        <!-- START of the zanox affiliate HTML code -->
        <!-- ( The HTML code may not be changed in the sense of faultless functionality! ) -->
        <a href="http://ad.zanox.com/ppc/?28670354C1203508154T"><img src="http://ad.zanox.com/ppv/?28670354C1203508154" align="bottom" width="300" height="250" border="0" hspace="1" alt="300 x 250"></a>
        <!-- ENDING of the zanox-affiliate HTML-Code -->


    </div>


</div>
<div class="row pagination pagination-centered">
    <?php echo $this->pagination->create_links(); ?>
</div>


<script>
    jQuery(document).ready(function(){

        jQuery(function() {
            jQuery("img.lazy").lazyload({
                load : init_masonry
            });

        });

    });
</script>