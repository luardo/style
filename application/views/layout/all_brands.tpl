{extends file="layout.tpl"}
{block name="content"}
<div class="row products">
    <ul class="breadcrumbs">
        <li><a href="{$baseurl}">Startseite</a></li>
        <li><a href="{$baseurl}">brand</a></li>
        <li class="current"><a href="#"></a></li>
    </ul>

    <h2 class="text-center">All Brands</h2>

    <hr>
    <div class="large-12 columns">

        <div class="row">
            <ul class="">
                {foreach $brands as $brand}
                <li>
                    <a href="{base_url()}shop/{$brand->url_key}" target="_blank">{$brand->name}</a>
                    {if isset($session)}
                        {if $session["username"] eq 'luardo123'}
                            <em><a class="delete" title="{$brand->id}" > | Delete</a></em>
                        {/if}
                    {/if}
                </li>
                {/foreach}
            </ul>

        </div>
    </div>
    <div class="row">

    </div>

</div>

<script>
    jQuery(document).ready(function(){
        jQuery('.delete').on('click', function(event){
            event.preventDefault();
            var brandId= jQuery(this).attr( "title" );
            var $this = jQuery(this);
            console.log(brandId + $this);
            var postData = {
                'brand' : brandId
            };
            if(postData != ""){
                jQuery.ajax({
                    type: "POST",
                    url: "http://localhost/stylistr/brands/brands/removeBrandProducts",
                    data: postData,
                    success: function(data){
                        $this.text("Deleted");
                        $this.addClass('success');
                        $this.closest('li').remove();
                    },
                    error: function(){
                        alert("Fail"+ brandId)
                    }
                });
            } else {
                alert("Insert Amount.");
            }

        });
    });
</script>
{/block}