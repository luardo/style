<div class="row products">
    <ul class="breadcrumbs">
        <li><a href="<?php echo base_url() ?>">Home</a></li>
        <li><a href="<?php echo base_url() ?>">Shop</a></li>
        <li class="current"><a href="#">Style Room</a></li>
    </ul>

    <h2 class="text-center"><?php echo $username ?>'s StyleRoom</h2>
    <h5 class="text-center">All new from <br />
    <?php


    foreach($brands as $brand):
        echo $brand->name." / ";
    endforeach;
    ?>
    </h5>
    <hr>
    <div id="container" class="large-9 columns">

        <div class="row">

            <?php foreach($products as $product): ?>
            <div class="box">
                <a href="<?php echo $product->product_link; ?>" target="_blank" >
                    <img src="<?php echo $product->product_image; ?>">
                </a>
                <div class="panel">
                    <div class="product-brand"><?php echo $product->name; ?></div>
                     <div class="product-name"><a href="<?php echo $product->product_link; ?>" target="_blank"><?php echo $product->product_name; ?></a></div>
                    <div class="product-price"><?php echo $product->product_price; ?> €</div>
                </div>
            </div>
            <?php endforeach ?>

        </div>
    </div>
    <div class="large-3 columns">
        <h4>Welcome back <?php echo $username ?></h4>
    </div>
    <div class="row">

    </div>

</div>