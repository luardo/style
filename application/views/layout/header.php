<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Latest fashion collections, season trends and designer fashion from all the world in one place - Styluv. Love it, style it</title>
    <link href='http://fonts.googleapis.com/css?family=Julius+Sans+One' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Hammersmith+One' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?php echo base_url();?>/assets/css/foundation.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>/assets/css/jquery.mCustomScrollbar.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>/assets/css/style.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>/assets/css/sprites.css" />
    <script src="<?php echo base_url();?>assets/js/vendor/modernizr.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>

    <script src="<?php echo base_url();?>assets/js/vendor/jquery.cookie.js"></script>
    <script src="<?php echo base_url();?>assets/js/vendor/jquery.lazyload.js"></script>
    <meta name="verification" content="adbf4ef53034297f79b11d03ef6d4c6d" />

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-52252970-1', 'styluv.de');
        ga('send', 'pageview');

    </script>

    <!-- KISSmetrics tracking snippet -->
    <script type="text/javascript">var _kmq = _kmq || [];
        var _kmk = _kmk || 'b296dc1a6b829846ea9162ba0ab78967bcaa28a6';
        function _kms(u){
            setTimeout(function(){
                var d = document, f = d.getElementsByTagName('script')[0],
                    s = d.createElement('script');
                s.type = 'text/javascript'; s.async = true; s.src = u;
                f.parentNode.insertBefore(s, f);
            }, 1);
        }
        _kms('//i.kissmetrics.com/i.js');
        _kms('//doug1izaerwt3.cloudfront.net/' + _kmk + '.1.js');
    </script>


</head>
<body>

<div class="contain-to-grid">
    <nav class="top-bar" data-topbar>


        <ul class="title-area">
            <li class="name">
               <a href="<?php echo base_url();?>"> <img src="<?php echo base_url();?>assets/img/styluv-logo.png" alt="Follow your favorite brands: Gucci, Chloe, Armani, Burbery and more at Styluv" /></a>
            </li>
        </ul>
        <section class="top-bar-section">

            <ul class="right">
                <?php $this->load->helper('navigation');
                echo get_header_navigation();
                ?>

            </ul>
        </section>

    </nav>
</div>