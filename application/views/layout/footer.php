<footer class="row">
    <div class="large-12 columns">
        <hr>

        <div class="row">
            <div class="large-6 columns">
                <p></p>
            </div>

            <div class="large-6 columns">
                <ul class="inline-list right">
                    <li>
                        <a href="<?php echo base_url()?>contact">Kontakt</a>
                    </li>

                    <li>
                        <a href="<?php echo base_url()?>about">Über uns</a>
                    </li>

                    <li>
                        <a href="<?php echo base_url()?>impressum">Impressum</a>
                    </li>

                </ul>
            </div>
        </div>
    </div>
</footer>


<script src="<?php echo base_url();?>assets/js/foundation.min.js"></script>
<script src="<?php echo base_url();?>assets/js/vendor/modernizr.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.masonry.js"></script>
<script>
    $(document).foundation();
    var $containter = $('#container');


    /**
     * Base js functions
     */

    $(document).ready(function(){
        //Init jQuery Masonry layout
        init_masonry();

    });


    function init_masonry(){
        var $container = $('#container');

        var gutter = 15;
        var min_width = 180;
        $container.imagesLoaded( function(){
            $container.masonry({
                itemSelector : '.box',
                gutterWidth: gutter,
                columnWidth: function( containerWidth ) {
                    var num_of_boxes = (containerWidth/min_width | 0);

                    var box_width = (((containerWidth - (num_of_boxes-1)*gutter)/num_of_boxes) | 0) ;

                    if (containerWidth < min_width) {
                        box_width = containerWidth;
                    }

                    $('.box').width(box_width);

                    return box_width;
                }
            });
        });
    }
    jQuery(".btn-follow").on('click',function(event ){
        event.preventDefault();
        var brandId= jQuery(this).attr( "title" );
        var $this = jQuery(this);

        var postData = {
            'brand' : brandId,
            'user' : 1
        };
        if(postData != ""){
            jQuery.ajax({
                type: "POST",
                url: "http://localhost/stylistr/user/followbrand",
                data: postData,
                success: function(data){
                    $this.text("Following");
                    $this.addClass('success');
                },
                error: function(){
                    alert("Fail")
                }
            });
        } else {
            alert("Insert Amount.");
        }
    });




</script>

</body>
</html>