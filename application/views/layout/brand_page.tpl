{extends file="layout.tpl"}
{block name="content"}
<div class="row products">
    <ul class="breadcrumbs">

    </ul>

    <h2 class="text-center"> {$products[0]->name}</h2>

    <hr>
    <div id="container" class="large-12 columns">

        <div class="row">
            {foreach $products as $product}
            <div class="box">
                <div class="image-container">
                    <a href="{$product->product_link}" target="_blank">
                        <img class="lazy" data-original="{$product->product_image_medium}"
                             src="{base_url()}assets/img/spinner.gif">
                    </a>
                </div>

                <div class="panel">
                    <div class="product-brand">{$product->name}</div>
                    <div class="product-name"><a href="{$product->product_link}" target="_blank">{$product->product_name}</a></div>

                    <div class="product-price">
                        {if isset($product->product_old_price)}
                        <span class="old-price">{$product->product_old_price}€</span>
                        Now: {$product->product_price} €
                        {else}
                        <?php {$product->product_price} €
                        {/if}
                    </div>
                </div>
                {if isset($session)}
                    {if $session["username"] eq 'luardo123'}
                        <button class="delete" value="{$product->id_product}">Delete</button>
                    {/if}
                {/if}
            </div>
            {/foreach}

        </div>
    </div>
    <div class="row">

    </div>

</div>
<div class="row pagination pagination-centered">
    {$pagination_helper}
</div>

    <script src="{$baseurl}/assets/js/jquery.masonry.js"></script>
    <script src="{$baseurl}/assets/js/scripts.js"></script>

<script>
    jQuery(document).ready(function(){

        jQuery(function() {
            jQuery("img.lazy").lazyload({
                load : init_masonry
            });

        });
        //jQuery("img.lazy").lazyload();

        jQuery('.delete').on('click', function(event){
            event.preventDefault();
            var brandId= jQuery(this).attr( "value" );
            var $this = jQuery(this);
            console.log(brandId + $this);
            var postData = {
                'brand' : brandId
            };
            if(postData != ""){
                jQuery.ajax({
                    type: "POST",
                    url: "http://localhost/stylistr/brands/brands/removeItem",
                    data: postData,
                    success: function(data){
                        $this.text("Deleted");
                        $this.addClass('success');
                        $this.closest('.box').remove();
                    },
                    error: function(){
                        alert("Fail"+ brandId)
                    }
                });
            } else {
                alert("Insert Amount.");
            }

        });
    });
</script>

{/block}