{extends file="layout.tpl"}
{block name="content"}
    <div class="showroom-header">
        <h2 class="text-center">My Style Feed</h2>

        {include file='menu_catalog.tpl'}

        <div class="navigation small-centered">
            <form method="get">
                <ul>
                    <li class="active">
                        <a href="#">Your style feed!</a>

                        <div class="box-filter scrollslider ">
                            <div class="search-filter">
                                <input type="text" placeholder="search" />
                            </div>
                            <div class="scroll-bar mCustomScrollbar" data-mcs-theme="dark-3">
                                <ul class="filters">
                                    {foreach $brands as $brand}
                                        <li><a href="?brand={$brand->brand_id}"> {$brand->name} </a></li>
                                    {/foreach}
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li>
                        <a href="#">Categories</a>
                        <div class="box-filter scrollslider ">
                            <div class="search-filter">
                                <input type="text" placeholder="search" />
                            </div>
                            <div class="scroll-bar mCustomScrollbar" data-mcs-theme="dark-3">
                                <ul class="filters">
                                    {foreach $categories as $category}
                                        <li><a href="?cat={$category->cat_id}"> {$category->cat_name} </a></li>
                                    {/foreach}
                                </ul>
                            </div>
                        </div>




                    </li>
                    <li><a href="#">Your Favorites</a> </li>
                    <li><a href="#">Your brands</a> </li>
                    <li><button value="1" name="sale" id="sale" type="submit">On Sale %</button>  </li>

                </ul>
            </form>
        </div>
    </div>

    <div class="row products">


        <div id="container" class="large-12 collapse right">


            <div class="row">

                <h3>Products you LUV</h3>


                {foreach $products as $product}
                <div class="box">
                    <div class="love-box">
                       <button class="btn luv-it" data="{$product->id_product}">LUV IT</button>
                        <br />
                        <a href="{base_url()}redirect/?id={$product->id_product}" target="_blank">
                        <button class="btn buy-it">BUY IT</button>
                        </a>
                    </div>

                    <div class="image-container" style="overflow: hidden">
                        <a href="{base_url()}redirect/?id={$product->id_product}" target="_blank">
                            <img class="lazy" data-original="{$product->product_image_medium}"
                                 src="{base_url()}assets/img/spinner.gif">
                        </a>
                    </div>

                    <div class="panel">
                        <div class="product-brand">{$product->name}</div>
                        <div class="product-name"><a href="{$product->id_product}"
                                                     target="_blank">{$product->product_name|truncate:35}</a></div>
                        <div class="product-price">
                            {if ($product->product_old_price > 0)}
                            <span class="old-price">{$product->product_old_price}€</span>
                            Now: {$product->product_price} €
                            {else}
                            {$product->product_price} €
                        {/if}
                        </div>

                            {if isset($session)}
                                {if $session["username"] eq 'luardo123'}
                                    <a class="luv" value="{$product->id_product}">Highlight</a>
                                    |
                                    <a class="delete" value="{$product->id_product}">Delete</a>
                                {/if}
                            {/if}

                        </div>

                    </div>
                {/foreach}
            </div>

            <div class="row">

                <h3>All LUV from the community</h3>


                {foreach $luv_products as $product}
                    <div class="box">
                        <div class="love-box">
                            <button class="btn luv-it" data="{$product->id_product}">LUV IT</button>
                            <br />
                            <a href="{base_url()}redirect/?id={$product->id_product}" target="_blank">
                                <button class="btn buy-it">BUY IT</button>
                            </a>
                        </div>

                        <div class="image-container" style="overflow: hidden">
                            <a href="{base_url()}redirect/?id={$product->id_product}" target="_blank">
                                <img class="lazy" data-original="{$product->product_image_medium}"
                                     src="{base_url()}assets/img/spinner.gif">
                            </a>
                        </div>

                        <div class="panel">
                            <div class="product-brand">{$product->name}</div>
                            <div class="product-name"><a href="{$product->id_product}"
                                                         target="_blank">{$product->product_name|truncate:35}</a></div>
                            <div class="product-price">
                                {if ($product->product_old_price > 0)}
                                    <span class="old-price">{$product->product_old_price}€</span>
                                    Now: {$product->product_price} €
                                {else}
                                    {$product->product_price} €
                                {/if}
                            </div>

                            {if isset($session)}
                                {if $session["username"] eq 'luardo123'}
                                    <a class="luv" value="{$product->id_product}">Highlight</a>
                                    |
                                    <a class="delete" value="{$product->id_product}">Delete</a>
                                {/if}
                            {/if}

                        </div>

                    </div>
                {/foreach}
            </div>

        </div>

    </div>
    <div class="row pagination pagination-centered">
        {$pagination_helper}
    </div>
    <script>
        jQuery(document).ready(function () {

            jQuery(function () {
                jQuery("img.lazy").lazyload({
                    load: init_masonry
                });

            });

        });
    </script>
    <script type="text/javascript">
        //initiating jQuery
        jQuery(function($) {
            $(document).ready( function() {
                //enabling stickUp on the '.navbar-wrapper' class
                $('.navigation').stickUp();
            });
        });

    </script>

{/block}