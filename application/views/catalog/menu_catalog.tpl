{extends file="favorites_catalog.tpl"}
<div class="navigation small-centered">
<form method="get">
        <ul>
            <li class="">
                <a href="{$baseurl}">Your style feed!</a>
                <div class="box-filter scrollslider ">
                    <div class="search-filter">
                        <input type="text" placeholder="search" />
                    </div>
                    <div class="scroll-bar mCustomScrollbar" data-mcs-theme="dark-3">
                        <ul class="filters">
                            {foreach $brands as $brand}
                                <li><a href="?brand={$brand->brand_id}"> {$brand->name} </a></li>
                            {/foreach}
                        </ul>
                    </div>
                </div>
            </li>

            <li>
                <a href="#">Categories</a>
                <div class="box-filter scrollslider ">
                    <div class="search-filter">
                        <input type="text" placeholder="search" />
                    </div>
                    <div class="scroll-bar mCustomScrollbar" data-mcs-theme="dark-3">
                        <ul class="filters">
                            {foreach $categories as $category}
                                <li><a href="?cat={$category->cat_id}"> {$category->cat_name} </a></li>
                            {/foreach}
                        </ul>
                    </div>
                </div>




            </li>
            <li><a href="#">Your Favorites</a> </li>
            <li><a href="#">Your brands</a>
                <div class="box-filter scrollslider ">
                    <div class="search-filter">
                        <input type="text" placeholder="search" />
                    </div>
                    <div class="scroll-bar mCustomScrollbar" data-mcs-theme="dark-3">
                        <ul class="filters">
                            {foreach $brands as $brand}
                                <li><a href="?brand={$brand->brand_id}"> {$brand->name} </a></li>
                            {/foreach}
                        </ul>
                    </div>
                </div>
            </li>


            <li><button value="1" name="sale" id="sale" type="submit">On Sale %</button>  </li>

        </ul>
    </form>
</div>