<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Latest fashion collections, season trends and designer fashion from all the world in one place - Styluv. Love it, style it</title>
    <link href='http://fonts.googleapis.com/css?family=Julius+Sans+One' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Hammersmith+One' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{$baseurl}/assets/css/foundation.css" />
    <link rel="stylesheet" href="{$baseurl}/assets/css/jquery.mCustomScrollbar.css" />
    <link rel="stylesheet" href="{$baseurl}/assets/css/style.css" />
    <link rel="stylesheet" href="{$baseurl}/assets/css/sprites.css" />
    <script src="{$baseurl}/assets/js/vendor/modernizr.js"></script>
    <script src="{$baseurl}/assets/js/jquery.min.js"></script>

    <script src="{$baseurl}/assets/js/vendor/jquery.cookie.js"></script>
    <script src="{$baseurl}/assets/js/vendor/jquery.lazyload.js"></script>
    <script src="{$baseurl}/assets/js/vendor/jquery.mCustomScrollbar.min.js"></script>
    <script src="{$baseurl}/assets/js/vendor/jquery.mousewheel.min.js"></script>
    <script src="{$baseurl}/assets/js/stickUp.min.js"></script>
    <meta name="verification" content="adbf4ef53034297f79b11d03ef6d4c6d" />





</head>
<body style="background: #f4f4f4">


<div class="contain-to-grid">
    <nav class="top-bar" data-topbar>


        <ul class="title-area">
            <li class="name">
                <a href="{$baseurl}"> <img src="{$baseurl}assets/img/logo-big.png" alt="Follow your favorite brands: Gucci, Chloe, Armani, Burbery and more at Styluv" /></a>
            </li>
        </ul>
        <section class="top-bar-section">

            <ul class="right">
                {ci helper='navigation' function='get_header_navigation'}
            </ul>
        </section>

    </nav>
</div>




    {block name="content"}{/block}


<footer class="row">
    <div class="large-12 columns">
        <hr>

        <div class="row">
            <div class="large-6 columns">
                <p></p>
            </div>

            <div class="large-6 columns">
                <ul class="inline-list right">
                    <li>
                        <a href="{$baseurl}contact">Kontakt</a>
                    </li>

                    <li>
                        <a href="{$baseurl}about">Über uns</a>
                    </li>

                    <li>
                        <a href="{$baseurl}impressum">Impressum</a>
                    </li>

                </ul>
            </div>
        </div>
    </div>
</footer>


<script src="{$baseurl}/assets/js/foundation.min.js"></script>
<script src="{$baseurl}/assets/js/jquery.masonry.js"></script>
<script src="{$baseurl}/assets/js/scripts.js"></script>




</body>
</html>