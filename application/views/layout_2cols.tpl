<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Latest fashion collections, season trends and designer fashion from all the world in one place - Styluv. Love it, style it</title>
    <link href='http://fonts.googleapis.com/css?family=Julius+Sans+One' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Hammersmith+One' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{$baseurl}/assets/css/foundation.css" />
    <link rel="stylesheet" href="{$baseurl}/assets/css/jquery.mCustomScrollbar.css" />
    <link rel="stylesheet" href="{$baseurl}/assets/css/style.css" />
    <link rel="stylesheet" href="{$baseurl}/assets/css/sprites.css" />
    <script src="{$baseurl}/assets/js/vendor/modernizr.js"></script>
    <script src="{$baseurl}/assets/js/jquery.min.js"></script>

    <script src="{$baseurl}/assets/js/vendor/jquery.cookie.js"></script>
    <script src="{$baseurl}/assets/js/vendor/jquery.lazyload.js"></script>
    <meta name="verification" content="adbf4ef53034297f79b11d03ef6d4c6d" />



    <!-- KISSmetrics tracking snippet -->
    <script type="text/javascript">var _kmq = _kmq || [];
        var _kmk = _kmk || 'b296dc1a6b829846ea9162ba0ab78967bcaa28a6';
        function _kms(u){
            setTimeout(function(){
                var d = document, f = d.getElementsByTagName('script')[0],
                        s = d.createElement('script');
                s.type = 'text/javascript'; s.async = true; s.src = u;
                f.parentNode.insertBefore(s, f);
            }, 1);
        }
        _kms('//i.kissmetrics.com/i.js');
        _kms('//doug1izaerwt3.cloudfront.net/' + _kmk + '.1.js');
    </script>


</head>
<body >

<div class="contain-to-grid">
    <nav class="top-bar" data-topbar>


        <ul class="title-area">
            <li class="name">
                <a href="{$baseurl}"> <img src="{$baseurl}assets/img/styluv-logo.png" alt="Follow your favorite brands: Gucci, Chloe, Armani, Burbery and more at Styluv" /></a>
            </li>
        </ul>
        <section class="top-bar-section">

            <ul class="right">
                {ci helper='navigation' function='get_header_navigation'}
            </ul>
        </section>

    </nav>
</div>


<div class="content">
    <div class="row">
        <ul class="breadcrumbs">
            <li><a href="<?php echo base_url() ?>">Startseite</a></li>
            <li class="current"><a href="#">Über uns</a></li>
        </ul>

        <div class="small-3 columns">
            {include file="layout/sidebar.tpl"}
        </div>

        <div class="small-9 columns">
            {block name="content"}{/block}
        </div>


    </div>

</div>


<footer class="row">
    <div class="large-12 columns">
        <hr>

        <div class="row">
            <div class="large-6 columns">
                <p></p>
            </div>

            <div class="large-6 columns">
                <ul class="inline-list right">
                    <li>
                        <a href="{$baseurl}contact">Kontakt</a>
                    </li>

                    <li>
                        <a href="{$baseurl}about">Über uns</a>
                    </li>

                    <li>
                        <a href="{$baseurl}impressum">Impressum</a>
                    </li>

                </ul>
            </div>
        </div>
    </div>
</footer>

<script src="{$baseurl}/assets/js/jquery.masonry.js"></script>
<script>
    $(document).foundation();
    var $containter = $('#container');


    /**
     * Base js functions
     */

    $(document).ready(function(){
        //Init jQuery Masonry layout
        init_masonry();

    });


    function init_masonry(){
        var $container = $('#container');

        var gutter = 15;
        var min_width = 180;
        $container.imagesLoaded( function(){
            $container.masonry({
                itemSelector : '.box',
                gutterWidth: gutter,
                columnWidth: function( containerWidth ) {
                    var num_of_boxes = (containerWidth/min_width | 0);

                    var box_width = (((containerWidth - (num_of_boxes-1)*gutter)/num_of_boxes) | 0) ;

                    if (containerWidth < min_width) {
                        box_width = containerWidth;
                    }

                    $('.box').width(box_width);

                    return box_width;
                }
            });
        });
    }
    jQuery(".btn-follow").on('click',function(event ){
        event.preventDefault();
        var brandId= jQuery(this).attr( "title" );
        var $this = jQuery(this);

        var postData = {
            'brand' : brandId,
            'user' : 1
        };
        if(postData != ""){
            jQuery.ajax({
                type: "POST",
                url: "http://localhost/stylistr/user/followbrand",
                data: postData,
                success: function(data){
                    $this.text("Following");
                    $this.addClass('success');
                },
                error: function(){
                    alert("Fail")
                }
            });
        } else {
            alert("Insert Amount.");
        }
    });




</script>

</body>
</html>