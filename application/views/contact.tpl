{extends file="layout_2cols.tpl"}
{block name="content"}

        <div class="small-5 columns">
            <h3>Contact</h3>

            <p>
                Melde Dich jederzeit gerne, wenn Du Fragen oder Anregungen zu unserem Angebot hast.  <br />
                <a href="">info(at)styluv.de</a>
            </p>

        </div>

        <div class="small-4 columns">
            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et
                dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet
                clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet,
                consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat,
                sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no
                sea takimata sanctus est Lorem ipsum dolor sit amet.</p>

        </div>


{/block}
