<?php

/**
 * Created by PhpStorm.
 * User: luis.gamero
 * Date: 05.07.14
 * Time: 16:29
 */
class Login_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function login($username, $password)
    {
        $this -> db -> select('id, username, password, role, gender');
        $this -> db -> from('users');
        $this -> db -> where('username', $username);
        $this -> db -> where('password', MD5($password));
        $this -> db -> limit(1);

        $query = $this -> db -> get();

        if($query -> num_rows() == 1)
        {
            return $query->result();
        }
        else
        {
            return false;
        }
    }

}