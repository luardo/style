<?php
/**
 * Created by PhpStorm.
 * User: luis.gamero
 * Date: 20.05.14
 * Time: 12:17
 */


class user_model extends CI_Model{

    public function __construct()
    {
        $this->load->database();
    }

    public function getUserById($id)
    {

    }

    public function getAllUsers()
    {

    }

    public function createUser($data)
    {
        if(isset($data['user_id']))
        {
            $this->db->where('id', $data['user_id']);
            $user = array(
                'email'=>$data["email"],
                'username'=>$data["username"],
                'password'=>md5($data["password"])
            );
            $this->db->update('users', $user);

            return $data['user_id'];

        }

        else
        {
            $user = array(
                'email'=>$data["email"],
                'gender'=>$data["gender"],
                'username'=>$data["username"],
                'password'=>md5($data["password"])
            );


            $this->db->insert('users',$user);
            $insert_id = $this->db->insert_id();
            $this->db->trans_complete();
            return  $insert_id;
        }

    }

    public function getUserProfile($user_id) {
        $this->db->select('*');
        $this->db->where('id', $user_id);
        $query = $this->db->get('users');
        $result = $query->row();
        return $result;
    }

    public function saveBrandToUser($userId, $brandId)
    {
        $data = array(
            'user_id'=>$userId,
            'brand_id'=>$brandId
        );
        return $this->db->insert('user_brand',$data);
    }

    public function saveStyleToUser($userId, $styleId)
    {
        $data = array(
            'user_id'=>$userId,
            'style_id'=>$styleId
        );
        return $this->db->insert('user_style',$data);
    }


    public function user_like_product($userId, $productId)
    {
        $data = array(
            'user_id'=>$userId,
            'product_id'=>$productId
        );
        return $this->db->insert('user_product',$data);

    }



} 