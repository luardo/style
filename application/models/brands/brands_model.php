<?php
/**
 * Created by PhpStorm.
 * User: luis.gamero
 * Date: 19.05.14
 * Time: 18:44
 */


class brands_model extends CI_Model {



    public function __construct()	{
        $this->load->database();
    }

    public function getBrands($limit) {
        $this->db->select('*');
        $this->db->order_by('id', 'RANDOM');
        $this->db->where('logo', 1);
        $query = $this->db->get('brand');
        $result = $query->result();
        return $result;
    }

    public function getAllBrands() {
        $this->db->select('*');
        $this->db->order_by('name', 'ASC');
        $this->db->where('id IN (SELECT product_brand_id from product)');
        $this->db->where('status',1);
        $this->db->where('brand_style >', 0);
        $query = $this->db->get('brand');
        $result = $query->result();
        return $result;
    }

    public function getQuizBrands($whereArray, $limit = null) {
        $this->db->select('id, name,url_key');
        $gender = 1;
        $this->db->order_by('id', 'RANDOM');
        $this->db->where('id IN (SELECT product_brand_id from product WHERE product_gender = '.$gender.' )');
        //$this->db->join('product', 'product.product_brand_id = brand.id', 'left');
        $this->db->where($whereArray);
        $query = $this->db->get('brand', $limit);
        $result = $query->result();
        return $result;
    }

    public function getByName($name) {
        $this->db->select('*');
        $this->db->where('name', $name);
        $query = $this->db->get('brand');
        $result = $query->row();
        if(!$result) {
            return false;
        }
        return $result->id;
    }

    public function getIdByUrl($url_key) {
        $this->db->select('id');
        $this->db->where('url_key', $url_key);
        $query = $this->db->get('brand');
        $result = $query->row();
        if(!$result) {
            return false;
        }
        return $result->id;
    }

    public function insertBrand($name) {

        if($this->getByName($name)) {
            return $this->getByName($name);
        }
        else {
        $brand = array(
            'name'=>$name,
            'description'=>'',
            'logo'=> '');

        $this->db->insert('brand',$brand);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return  $insert_id;
        }
    }

    public function updateBrand($brand){
        $this->db->where('id', $brand['id']);
        $this->db->update('brand', $brand);
        return $brand['id']." updated <br />";
    }

    public function deletebrandproducts($brandId){
        //$this->db->delete('product', array('product_brand_id' => $brandId));
        //$this->db->delete('brand', array('id' => $brandId));

        $this->db->where('product_brand_id', $brandId);
        $this->db->update('product', array('product_status' => 0));

        $this->db->where('id', $brandId);
        $this->db->update('brand', array('status' => 0));


        return true;
    }

} 