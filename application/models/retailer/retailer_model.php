<?php
/**
 * Created by PhpStorm.
 * User: luis.gamero
 * Date: 19.05.14
 * Time: 18:44
 */


class retailer_model extends CI_Model {


    public function insertRetailer($name) {

        if($this->getByName($name)) {
            return $this->getByName($name);
        }
        else {
            $brand = array(
                'shop_name'=>$name
                );

            $this->db->insert('shop',$brand);
            $insert_id = $this->db->insert_id();
            $this->db->trans_complete();
            return  $insert_id;
        }
    }


    public function getByName($name) {
        $this->db->select('*');
        $this->db->where('shop_name', $name);
        $query = $this->db->get('shop');
        $result = $query->row();
        if(!$result) {
            return false;
        }
        return $result->shop_id;
    }


}