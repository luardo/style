<?php
/**
 * Created by PhpStorm.
 * User: luis.gamero
 * Date: 20.06.14
 * Time: 14:23
 */



class category_model extends CI_Model {

    public function __construct()	{
        $this->load->database();
    }


    public function getCategoryId_ByString($MerchantProductCategory) {
        $this->db->select('*');

        $this->db->where('LOWER(cat_name)', strtolower($MerchantProductCategory));
        $this->db->like('LOWER(cat_synonyms)', 'strtolower($MerchantProductCategory)');
        $query = $this->db->get('categories');
        $result = $query->result_array();
        if($result){
            echo $result[0]['cat_name'];
            return $result[0]['cat_id'];
        }

    }


    public function get_all_categories() {
        $this->db->select('*');
        $this->db->order_by('cat_name', 'ASC');
        $this->db->where('cat_id IN (SELECT product_category from product)');
        $query = $this->db->get('categories');
        $result = $query->result();
        return $result;
    }


}