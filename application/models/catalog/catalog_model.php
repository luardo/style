<?php
/**
 * Created by PhpStorm.
 * User: luis.gamero
 * Date: 20.06.14
 * Time: 14:23
 */



class catalog_model extends CI_Model {

    public function __construct()	{
        $this->load->database();
    }

    /**
     * @return mixed
     * get the user products macthing the brands he likes
     */

    public function getUserBrands($user_id) {
        $this->db->select('brand_id');
        $this->db->select('name');
        $this->db->where('user_id', $user_id);
        $this->db->join('brand', 'user_brand.brand_id = brand.id','inner');
        $query = $this->db->get('user_brand');
        $result = $query->result();
        return $result;
    }




    /**
     * @param $product
     * Save the products from the CSV file
     */

    public function saveProducts($data) {
        $result =  $this->db->insert('product',$data);
        if($result){
            $insert_id = $this->db->insert_id();
            $this->db->trans_complete();
            echo  $insert_id. "ID inserted <br />";
            return $result;
        }
        else {
            echo "fail to insert";
        }

    }



    /**
     * Get all the products machting the  $userBrands
     * @param $userBrands = brands that user follows
     * @return mixed
     */

    public function getProducts($userId, $limit, $offset, $queryArray = null){

    $CI =& get_instance();
    $CI->load->model('user/user_model');

    $this->db->select('*');

    $userBrands = $this->getUserBrands($userId);
    $user = $CI->user_model->getUserProfile($userId);



        foreach ($userBrands as $key => $userBrand) {
            $this->db->or_where('product_brand_id', $userBrand->brand_id);
            $this->db->where('product_status', 1);
            if ($queryArray != null) {
                $this->db->where($queryArray);
            }
            $this->db->where($queryArray);
            $this->db->where('product_gender', $user->gender);

        }

        $this->db->join('brand', 'brand.id = product.product_brand_id', 'left');
        $this->db->distinct();
        $this->db->group_by('product.id_product');
        $this->db->order_by('product.product_relevance DESC, product.product_UpdateDate DESC');


    $query = $this->db->get('product', $offset, $limit);
    $result = $query->result();
   // echo $this->db->last_query();
    shuffle($result);
    return $result;
    }

    public function get_new_Products($userId, $limit, $offset, $queryArray = null){

        $CI =& get_instance();
        $CI->load->model('user/user_model');
        $user = $CI->user_model->getUserProfile($userId);

        $this->db->select('*');

        if ($queryArray != null) {
            $this->db->where($queryArray);
        }

        $this->db->where('product_status', 1);
        $this->db->where('product_gender', $user->gender);
        $ignored_categories = array(7, 29);

        $this->db->where_not_in('product_category', $ignored_categories);

        $this->db->join('brand', 'brand.id = product.product_brand_id', 'left');
        $this->db->distinct();
        $this->db->group_by('product.id_product');
        $this->db->order_by('product.product_relevance DESC, product.product_UpdateDate DESC');


        $query = $this->db->get('product', $offset, $limit);
        $result = $query->result();
       // echo $this->db->last_query();
        return $result;
    }


    public function getProducts_byStyle($userId, $limit, $offset, $queryArray = null) {
        $CI =& get_instance();
        $CI->load->model('user/user_model');

        $this->db->select('*');
        $userBrands = $this->getUserBrands($userId);
        $user = $CI->user_model->getUserProfile($userId);

        $styles = $this->get_user_styles($user->id);

        foreach($styles as $style) {
            $this->db->or_where('id_style', $style->style_id);
        }

        $this->db->join('product', 'product.id_product = product_styles.id_product', 'left');
        $this->db->join('brand', 'brand.id = product.product_brand_id', 'left');
        $this->db->distinct();
        $this->db->group_by('product.id_product');

        foreach ($userBrands as $key => $userBrand) {
            $this->db->or_where('product_brand_id', $userBrand->brand_id);
            $this->db->where('product_status', 1);
            if ($queryArray != null) {
                $this->db->where($queryArray);
            }
            $this->db->where('product_gender', $user->gender);
        }

        $this->db->order_by('product.product_relevance DESC, product.product_UpdateDate DESC');
       // $this->db->order_by('product.product_relevance', 'DESC');

        $query = $this->db->get('product_styles', $offset, $limit);

        $result = $query->result();
        shuffle($result);

        return $result;




    }

    public function get_user_styles($user_id){

        $this->db->select('*');
        $this->db->where('user_id', $user_id);
        $query = $this->db->get('user_style');
        return $query->result();

    }






    public function getProductsCount($userId){
        $CI =& get_instance();
        $CI->load->model('user/user_model');

        $this->db->select('*');
        $this->db->limit('*');

        $userBrands = $this->getUserBrands($userId);
        $user = $CI->user_model->getUserProfile($userId);

        foreach($userBrands as $key => $userBrand)
        {
            $this->db->or_where('product_brand_id',$userBrand->brand_id);
            $this->db->where('product_gender',$user->gender);
        }

        $this->db->join('brand', 'brand.id = product.product_brand_id', 'left');
        $query = $this->db->get('product');
        echo $query->num_rows();
        return  $query->num_rows();
    }



    protected function getBrandId($userBrand) {

        if(isset($userBrand['brand_id'])){

            return $userBrand['brand_id'];
        }
        else {
            return $userBrand->brand_id;
        }

    }

    /**
     * @param $brand_id
     * @return mixed
     * Get all products from a Brand by ID
     */
    public function getProductsByBrand($queryArray,$limit=null, $offset=null) {
        $this->db->select('*');
        $this->db->where($queryArray);
        $this->db->order_by('product.product_brand_id', 'RANDOM');
        $this->db->join('brand', 'brand.id = product.product_brand_id', 'left');
        $this->db->where('product.product_status',1);
        $this->db->group_by('product.id_product');
        $query = $this->db->get('product', $offset, $limit);
        $result = $query->result();
        return $result;
    }

    public function isProductDuplicated($data) {
        $this->db->select('*');
        $this->db->where('product_MerchantProductNumber', $data['product_MerchantProductNumber']);
        $query = $this->db->get('product');
        $result = $query->row();
        if($result){
            //returns if product exist by product_MerchantProductNumber
            return $result->id_product;
        }
        else {
            //check if product exist alreay but brand id and image url
            $this->db->select('*');
            $this->db->where('product_brand_id', $data['product_brand_id']);
            $this->db->where('product_image_medium', $data['product_image_medium']);
            $query = $this->db->get('product');
            $result = $query->row();
            if(!$result){
                return false;
            }
            else {
                return $result->id_product;
            }

        }

    }

    public function get_favorite_products($userId=null)
    {
        $this->db->select('*');
        if($userId){
            $this->db->where('user_id', $userId);
        }

        $this->db->join('product', 'product.id_product = user_product.product_id', 'left');
        $this->db->join('brand', 'brand.id = product.product_brand_id', 'left');
        $this->db->where('product.product_status',1);
        $this->db->distinct();
        $this->db->group_by('product.id_product');
        $query = $this->db->get('user_product');
        $result = $query->result();
        return $result;

    }

    public function getProduct_byId($id){
        $this->db->select('*');
        $this->db->where('id_product', $id);
        $query = $this->db->get('product');
        $result = $query->row();
        if($result){
            return $result;
        }
        else {
            return false;
        }


    }

    public function updateProducts($id, $data){

        $product_status = $this->getProductStatus($id);

        if(!$product_status){
            $this->db->where('id_product', $id);
            $this->db->update('product', $data);
            echo  $id. "ID updated <br />";
            return true;
        }
        return false;

    }

    protected function getProductStatus($id) {
        $this->db->select('*');
        $this->db->where('product_status', $id);
        $query = $this->db->get('product');
        $result = $query->row();
        if($result){
            return $result->product_status;
        }
        else {
            return false;
        }

    }



    public function getNewProducts($userBrand) {

    }

    public function delete($id) {
        var_dump($id);
        //$this->db->delete('product', array('id_product' => $id));
        $this->db->where('id_product', $id);
        $this->db->update('product', array('product_status' => 0));
        echo $this->db->last_query();
        return true;
    }

}