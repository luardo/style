<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class CSVReader {

    var $fields;            /** columns names retrieved after parsing */
    var $separator = '#';    /** separator used to explode each line */
    var $enclosure = "'";    /** enclosure used to decorate each field */

    var $max_row_size = 8096;    /** maximum row size to be used for decoding */

    function parse_file($p_Filepath) {

        $file = fopen($p_Filepath, 'r');
        /*$curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_FILE, $file);*/

        $this->fields = fgetcsv($file, $this->max_row_size, $this->separator, $this->enclosure);
        $keys_values = explode(',',$this->fields[0]);

        $content    =   array();
        $keys   =   $this->escape_string($keys_values);
        $keys   =   $this->remove_utf8_bom($keys);

        $i  =   1;
        while( ($row = fgetcsv($file, $this->max_row_size, $this->separator, $this->enclosure)) != false ) {
            if( $row != null ) { // skip empty lines

                //$values =   explode(',',$row[0]);
                $values = str_getcsv($row[0], ",", "'");
                if(count($keys) == count($values)){

                    $arr    =   array();
                    $new_values =   array();
                    $new_values =   $this->escape_string($values);

                    for($j=0;$j<count($keys);$j++){
                        if($keys[$j] != ""){
                            $arr[$keys[$j]] =   $new_values[$j];
                        }
                    }

                    $content[$i]=   $arr;

                    $i++;
                }
            }
        }
        fclose($file);
        return $content;
    }

    function escape_string($data){
        $result =   array();
        foreach($data as $row){
            $row = html_entity_decode($row, ENT_QUOTES, "UTF-8");
            $result[]   =   str_replace("'", '',$row);
        }
        return $result;
    }

    function remove_utf8_bom($text)
    {
        $bom = pack('H*','EFBBBF');
        $text = preg_replace("/^$bom/", '', $text);
        return $text;
    }
}