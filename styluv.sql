-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server Version:               5.6.14 - MySQL Community Server (GPL)
-- Server Betriebssystem:        Win32
-- HeidiSQL Version:             8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
-- Exportiere Daten aus Tabelle styluv.brand: ~56 rows (ungefähr)
/*!40000 ALTER TABLE `brand` DISABLE KEYS */;
REPLACE INTO `brand` (`id`, `name`, `description`, `image`, `logo`) VALUES
	(1, 'Burberry', NULL, 'burberry', '1'),
	(2, 'Kate Spade', NULL, NULL, NULL),
	(3, 'Coach', NULL, NULL, NULL),
	(4, 'Gucci', NULL, 'gucci', '1'),
	(5, 'Dolce & Gabbana', NULL, NULL, ''),
	(6, 'Tory Burch', NULL, NULL, NULL),
	(7, 'Ralph Lauren', NULL, NULL, '1'),
	(8, 'Hugo Boss', NULL, NULL, NULL),
	(10, 'Michael Kors', NULL, NULL, NULL),
	(11, 'DKNY', NULL, NULL, '1'),
	(12, 'Diane von Furstenberg', NULL, NULL, NULL),
	(13, 'Calvin Klein', NULL, NULL, '1'),
	(14, 'Marc Jacobs', NULL, NULL, '1'),
	(15, 'John Varvatos', NULL, NULL, NULL),
	(16, 'Paul Smith', NULL, NULL, NULL),
	(17, 'Alexander McQueen', NULL, NULL, NULL),
	(18, 'Jimmy Choo', NULL, NULL, '1'),
	(19, 'Chanel', NULL, NULL, '1'),
	(20, 'Yves Saint Laurent', NULL, NULL, '1'),
	(21, 'Ermenegildo Zegna', NULL, NULL, NULL),
	(22, 'Christian Dior', NULL, NULL, '1'),
	(23, 'Stuart Weitzman', NULL, NULL, NULL),
	(24, 'Christian Louboutin', NULL, NULL, NULL),
	(25, 'Giorgio Armani', NULL, NULL, '1'),
	(26, 'Bottega Veneta', NULL, NULL, '1'),
	(27, 'Oscar de La Renta', NULL, NULL, NULL),
	(28, 'Cole Haan', NULL, NULL, NULL),
	(29, 'Fendi', NULL, NULL, '1'),
	(30, 'Theory', NULL, NULL, NULL),
	(31, 'Roberto Cavalli', NULL, NULL, '1'),
	(32, 'Valentino', NULL, NULL, '1'),
	(33, 'Mulberry', NULL, NULL, NULL),
	(34, 'Hermes Paris', NULL, NULL, NULL),
	(35, 'Versace', NULL, NULL, NULL),
	(36, 'Givenchy', NULL, NULL, NULL),
	(37, 'Chloe', NULL, NULL, '1'),
	(38, 'Prada', NULL, NULL, '1'),
	(39, 'Zara', NULL, NULL, '1'),
	(40, 'Nike', NULL, NULL, NULL),
	(41, 'H&M', NULL, NULL, NULL),
	(42, 'Adidas', NULL, NULL, NULL),
	(43, 'Polo', NULL, NULL, NULL),
	(44, 'Asos', NULL, NULL, NULL),
	(45, 'Forever 21', NULL, NULL, NULL),
	(46, 'Hermes', NULL, NULL, NULL),
	(47, 'Louis Vuitton', NULL, NULL, '1'),
	(48, 'Converse', NULL, NULL, NULL),
	(49, 'Dior', NULL, NULL, '1'),
	(50, 'Levi’s', NULL, NULL, NULL),
	(51, 'Saint Laurent', NULL, NULL, NULL),
	(52, 'Victoria’s Secret', NULL, NULL, NULL),
	(53, 'Alexander Wang', NULL, NULL, NULL),
	(54, 'Vans', NULL, NULL, NULL),
	(55, 'Pull & Bear', NULL, NULL, NULL),
	(56, 'Berschka', NULL, NULL, NULL),
	(57, 'Nasty Gal', NULL, NULL, NULL);
/*!40000 ALTER TABLE `brand` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
