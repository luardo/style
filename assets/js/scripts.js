

var $containter = $('#container');
var baseUrl = 'http://www.styluv.loc/';


/**
 * Base js functions
 */

jQuery(document).ready(function () {
    //Init jQuery Masonry layout
    init_masonry();

    jQuery(function () {
        jQuery("img.lazy").lazyload({
            load: init_masonry
        });

    });


    jQuery(".box-filter").mCustomScrollbar();




    jQuery('li').click(function(){
        jQuery(this).find('.box-filter').addClass('active');

    });

    jQuery('.love-box .luv-it').click(function() {
        var self = jQuery(this);
        var product_id = self.attr('data');
        luv.addLuv(self,product_id);
    });


});


function init_masonry() {
    var $container = $('#containerö');

    var gutter = 0;
    var min_width = 180;
    $container.imagesLoaded(function () {
        $container.masonry({
            itemSelector: '.bosx',
            gutterWidth: gutter,
            columnWidth: function (containerWidth) {
                var num_of_boxes = (containerWidth / min_width | 0);

                var box_width = (((containerWidth - (num_of_boxes - 1) * gutter) / num_of_boxes) | 0);

                if (containerWidth < min_width) {
                    box_width = containerWidth;
                }

                $('.box').width(box_width);

                return box_width;
            }
        });
    });
}

jQuery(".btn-follow").on('click', function (event) {
    event.preventDefault();
    var brandId = jQuery(this).parent().find('input.brand').val();
    var $this = jQuery(this);

    var postData = {
        'brand': brandId,
        'user': jQuery(this).parent().find('input.user').val()
    };
    if (postData != "") {
        jQuery.ajax({
            type: "POST",
            url: "/register/quiz/followbrand",
            data: postData,
            success: function (data) {
                $this.text("Following");
                $this.addClass('success');
            },
            error: function () {
                alert("Fail")
            }
        });
    } else {
        alert("Insert Amount.");
    }
});

jQuery('.delete').on('click', function(event){
    event.preventDefault();
    var brandId= jQuery(this).attr( "value" );
    var $this = jQuery(this);
    console.log(brandId + $this);
    var postData = {
        'brand' : brandId
    };
    if(postData != ""){
        jQuery.ajax({
            type: "POST",
            url: "/brands/brands/removeItem",
            data: postData,
            success: function(data){
                $this.text("Deleted");
                $this.addClass('success');
                $this.closest('.box').remove();
            },
            error: function(){
                alert("Fail"+ brandId)
            }
        });
    } else {
        alert("Insert Amount.");
    }

});

var luv = {
    addLuv : function (self, id) {
        var postData = {
            'productId' : id
        };
        if(postData != ""){
            jQuery.ajax({
                type: "POST",
                url: "/catalog/showroom/user_likes_product",
                data: postData,
                success: function(data){
                    alert('success');
                },
                error: function(){
                    alert("Fail"+ id)
                }
            });
        } else {
            alert("Insert Amount.");
        }
    }
}